=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

# This module is responsible for storing user 
# records and providing password based auth 
# strategy
#
# Author:: Anton Nesterov (mailto:arch.nesterov@gmail.com)
# Copyright:: Copyright (c) 2018
# License:: BSD

require "redis"
require "colorize"
require "json"
require "bcrypt"
require 'securerandom'
require "singleton"
require_relative "../config"
require 'set'

# This class provides basic methods for manipulating 
# user records.
# User storage is Redis[0] by default. 
# Client relies on 'REDIS_URL' config variable
# == Example:
# --
# auth = Auth.new
# user = auth.user(:login => 'test', :password => 'test')
# user.create
# user.update(:password => '1234')
# check_auth = auth.user(:login => 'test', :password => '1234').valid
# users = auth.user.find(:query => "te")
# someUserData = auth.user(:login => "test").get 
# --

class Auth
	
	include Singleton

	# User methods are here
	class User
		
		include BCrypt

		def initialize(opts, redis)
			@redis = redis
			if opts
				@login = "user:#{opts[:login]}"
				@opts = opts
				@user = get()
			end
		end

		#Returns a redis key.
		def login
			@login
		end

		#Check if an user exists
		def exists
			return true if @redis.get(@login)
			return false
		end

		#Find users
		# * Available options:
		#	- :query => String
		#   - :cursor => Integer
		# * Returns list of redis keys 
		def find(opt)
			str = opt[:query]
			cursor = opt[:cursor] || 0
			if str
				all_keys = []
				loop {
			        cursor, keys = @redis.scan(cursor, :match => "user:*#{str}*")
			        all_keys += keys
			        break if cursor == "0"
			     }
				return all_keys
			else
				return @redis.scan(cursor)[1]
			end
		end


		#Get current user data
		# * Returns a json document
		# * Returns false if no user
		def get
			user = @redis.get(@login)
			return JSON.parse(user) if user
			false
		end

		#Create an user
		# * returns false if user exists
		def create
			if exists()
				puts "[user db] User |#{@opts[:login]}| already exists".red
				return false
			end
			begin
				data = hashPassword @opts
				@redis.set @login, data.to_json
				@redis.save
				puts "[user db] Created new user |#{@opts[:login]}|".green
				return true
			rescue Exception => e
				puts "[user db] Couldn't create new user |#{@opts[:login]}|".red
				puts e.message.red
			end
		end

		#Update user record
		# * If changing login, the old record will be removed
		# * Returns false if user doesnt exists
		def update(data)
			unless exists()
				puts "[user db] Cannot commit update. User |#{@opts[:login]}| does not exists.".red
				return false
			end
			begin
				puts "[user db] Updating user |#{@opts[:login]}|".yellow
				password = @user["password"]
				password = Password.create(data[:password]) if data[:password]
				remove() if !data[:login].nil? && data[:login] != @opts[:login]
				data[:password] = password
				@login = @login || "user:#{data[:login]}"
				@redis.set(@login, data.to_json)
				@redis.save
				@user = @redis.get(@login)
				puts "[user db] User updated |#{@opts[:login]}|".green
				return true
			rescue Exception => e
				puts "[user db] Error updating |#{@opts[:login]}|".red
				puts e.message.red
			end
			
		end

		#Remove curent user
		# * returns boolean
		def remove
			u = get()
			@redis.del(u["email"]) if u["email"]
			@redis.del(@login)
			@redis.save
			true
		end

		#Check if password is valid
		# * Returns boolean
		def valid
			if @user
				u = get()
				return  Password.new(u["password"]) == @opts[:password]
			else
				return false
			end
			return false
		end

		#Return bcrypt hash
		def hashPassword(opt)
			opt[:password] = Password.create(opt[:password])
			return opt
		end

		def checkPassword(hash)
			Password.new(hash) == @opts[:password]
		end

		def generateToken
			SecureRandom.uuid
		end

		def getTokenData(token)
			@redis.get("token:#{token}")
		end

		def setTokenData(token, data)
			@redis.set("token:#{token}", data)
			@redis.expire("token:#{token}", 3600)
		end

		def saveEmail(email, data = 0)
			unless @redis.get(email).nil?
				return false
			else
				@redis.set(email, data)
				return true
			end
		end

		def setEmailData(email, data)
			@redis.set(email, data)
		end

		def getEmailData(email)
			@redis.get(email)
		end

		def removeEmail(email)
			if @redis.get(email).nil?
				return false
			else
				return @redis.del(email) != 0
			end
		end

		def online
			@redis.set("online:#{@login}", "1")
			@redis.expire("online:#{@login}", 120)
		end

		def online?
			@redis.get("online:#{@login}").nil?
		end

	end

	def initialize
		Thread.new {
			while true
				break if connect()
				sleep 1
				puts "[auth provider] Could not connect to redis".red if @hits > 5
			end
			puts "[auth provider] Connected to redis".green
		}
	end

	def connect
		@hits = @hits || 1
		@hits += 1
		if @connection
			begin
				@connection.ping
				return @connection
			rescue Exception => e
				puts e.message.red if @hits > 5
				return false
			end
		end
		begin
			@connection = Redis.new(host: REDIS_HOST, port: REDIS_PORT , timeout: 1, db: 0)
			@connection.ping
			return @connection
		rescue Exception => e
			puts e.message.red if @hits > 5
			return false
		end	
	end

	def redis
		@connection
	end

	def user(opts = nil)
		return User.new(opts, redis())
	end



end