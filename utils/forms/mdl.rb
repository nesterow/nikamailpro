=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require 'ostruct'
require 'securerandom'

class MDLForm
	
	class Option
		attr_reader :title, :value, :checked
		def initialize(title, value, checked = false)
			@title = title
			@value = value
			@checked = checked
		end
	end

	def self.id i
		@id = i
	end
	
	def self.redirect l
		@redirect = l
	end

	def self.drawer b
		@drawer = b
	end
	
	def self.fields
		@fields ||= []
	end

	def self.title a
		@title = a
	end

	def self.button a
		@button = a
	end

	def self.action a
		@action = a
	end

	def self.method a
		@method = a
	end

	def self.input(title, opts)
		opts = OpenStruct.new(opts)
		if opts.type == :hidden
			html = %{<input placeholder="#{title}" type="#{opts.type}" id="#{opts.name}" name="#{opts.name}" value="#{opts.value}" />}
		elsif opts.type == :file
			html = %{
				<label>#{title}<br>
					<input type="#{opts.type}" id="#{opts.name}" name="#{opts.name}" value="#{opts.value}" />
				</label>
				<br>
				}
		else
			html = %{
				<div class="mdl-textfield mdl-js-textfield #{opts.floating && 'mdl-textfield--floating-label'}">
					<input class="mdl-textfield__input #{opts.required && 'required' || ''}" type="#{opts.type}" id="#{opts.name}" name="#{opts.name}" pattern="#{opts.pattern || '.*'}" message="#{opts.error}"/>
					<label class="mdl-textfield__label" for="#{opts.name}">#{title}</label>
					<span class="mdl-textfield__error">#{opts.error}</span>
				</div>
			}
		end
		fields.push(html)
	end

	def self.textarea(title, opts)
		opts = OpenStruct.new(opts)
		html = %{
			<div class="mdl-textfield mdl-js-textfield">
		    <textarea class="mdl-textfield__input #{opts.required && 'required' || ''}" type="text" rows="#{opts.rows}" id="#{opts.name}" name="#{opts.name}" message="#{opts.error}"></textarea>
		    <label class="mdl-textfield__label" for="#{opts.name}">#{title}</label>
		  	</div>
		}
		fields.push(html)
	end

	def self.checkbox(title, opts)
		opts = OpenStruct.new(opts)
		html = %{
			<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="#{opts.name}">
			  <input type="checkbox" id="#{opts.name}" name="#{opts.name}" class="mdl-checkbox__input" #{opts.checked && 'checked' || ''}>
			  <span class="mdl-checkbox__label">#{title}</span>
			</label>
		}
		fields.push(html)
	end

	
	def self.option(t, v, c = false)
		return Option.new(t, v,c)
	end

	def self.radio(*data, **opts)
		opts = OpenStruct.new(opts)
		html = "<h6 style='margin-bottom: 0px;'>#{opts.title}</h6>"
		count = 0
		data.each { |o| 
			count+=1
			html += %{
			<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="#{opts.name}#{count}#{@id}">
			  <input type="radio" id="#{opts.name}#{count}#{@id}" class="mdl-radio__button" name="#{opts.name}" value="#{o.value}" #{o.checked && 'checked' || ''}>
			  <span class="mdl-radio__label">#{o.title}</span>
			</label>
			<br>
			}
		}
		html += "<br>"
		fields.push(html)
	end

	def self.switch(title, opts)
		opts = OpenStruct.new(opts)
		html = %{
			<label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="#{opts.name}">
			  <input type="checkbox" id="#{opts.name}" name="#{opts.name}" class="mdl-switch__input" #{opts.checked && 'checked' || ''}>
			  <span class="mdl-switch__label">#{title}</span>
			</label>
		}
		fields.push(html)
	end

	def self.select(*data, **opts)
		opts = OpenStruct.new(opts)
		options = ""
		data.each { |o| 
			options+=%{<li class="mdl-menu__item" data-val="#{o.value}">#{o.title}</li>}
		}
		html = %{
		<div class="mdl-textfield mdl-js-textfield getmdl-select getmdl-select__fix-height">
        	<input type="text" value="" class="mdl-textfield__input" id="#{opts.name}" readonly>
        	<input type="hidden" value="" name="#{opts.name}">
        	<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
        	<label for="#{opts.name}" class="mdl-textfield__label">#{opts.title}</label>
        	<ul for="#{opts.name}" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
            	#{options}
        	</ul>
   		</div>
		}
		fields.push(html)
	end
	
	def self.text t
		fields.push "<p style='margin:2px; margin-top:1em;text-transform:uppercase;'><b>#{t}</b></p>"
	end
	
	def self.br
		fields.push '<br>'
	end

	def self.render

		id = @id || SecureRandom.uuid[0..5]
		html = %{
			<div class="mdl-card #{id}">
		}
		if @title
			html += %{
				<div class="mdl-card__title mdl-color--primary mdl-color-text--white">
					<h2 class="mdl-card__title-text">
						<a class="close-drawer" id="close-drawer-#{id}">&times;</a>
						#{@title}
					</h2>
				</div>
			}
		end
		html += %{
			<div class="mdl-card__supporting-text">
			<form action='#{@action}' method='#{@method}' id="#{id}" >
		}
		fields.each { |f|  
			html += f
		}
		html += %{
			</form>
			</div>
		}
		html += %{
			<div class="mdl-card__actions mdl-card--border gen-form">
			<button class="mdl-button mdl-button--raised mdl-button--colored mdl-js-button mdl-js-ripple-effect #{id}-submit">#{@button || 'Submit'}</button>
			</div>
		</div>
		<script>L(function(){InitForm("#{id}", "#{@redirect && @redirect || 'none'}");});</script>
		}

		if @drawer
			html = %{
				<div class="mdl-layout__drawer-right drawer-#{id} form-drawer">
				#{html}
        		</div>
        		<div class="mdl-layout__obfuscator-right drawer-ob-#{id}"></div>
        		<script>
        		L(function(){
        			window.activate_form_#{id} = function(){
        				$(".drawer-#{id}").addClass("dr-active");
  						$(".drawer-ob-#{id}").addClass("ob-active");
        			};
        			window.deactivate_form_#{id} = function(){
        				$(".drawer-#{id}").removeClass("dr-active");
  						$(".drawer-ob-#{id}").removeClass("ob-active");
        			};
        			$(".mdl-layout__obfuscator-right").click(window.deactivate_form_#{id});
        			$("#close-drawer-#{id}").click(window.deactivate_form_#{id});
        			$(document).keyup(function(e) {
					     if (e.keyCode == 27) { 
					     	window.deactivate_form_#{id}();
					    }
					});
        		});
        		</script>
			}
		end

		return html
	end

end