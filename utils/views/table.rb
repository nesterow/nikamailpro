=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require 'ostruct'

class DataTable

	def self.id name
		@id=name 
	end
	
	def self.api url
		@api=url
	end

	def self.column(**opts)
		@columns = @columns || []
		@columns.push OpenStruct.new(opts)
	end

	def self.col field, title, default = nil
		column :field => field, :title=>title, :default=>default
	end 

	def self.action title, cb
		@actions = @actions || []
		@actions.push OpenStruct.new(:title=>title, :callback=>cb)
	end

	def self.button title, cb
		@buttons = @buttons || []
		@buttons.push OpenStruct.new(:title=>title, :callback=>cb)
	end

	def self.form n
		@form = n
	end

	def self.render

		cols = ""

		@columns.each { |c|
			cols += "{ data: '#{c.field}', title: '#{c.title}', defaultContent:'#{c.default || ''}'  },"
		}

		(@actions || []).each { |b| 
			cols += %{{ data: null, width:"50px", className: "padding-0", defaultContent:'<a href="#" class="mdl-button mdl-button--colored" onclick="#{b.callback}(event)">#{b.title}</a>'},}
		}

		btns = ""
		(@buttons || []).each { |b| 
			btns += %{<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="#{b.callback}(event)">#{b.title}</button>}
		}

		html = %{
			<table id="#{@id}" class="mdl-data-table" cellspacing="0" width="100%">
			</table>
			<script>
			L(function(){
		        window.table_#{@id} = $('##{@id}').DataTable({
		            serverSide: true,
		            "ordering": false,
		            "dom": '<"toolbar">frtip',
		            columns: [
		               #{cols}
		            ],
		            "ajax": {
		                url: '#{@api}',
		            }
        		});
        		$("div.toolbar").html('#{btns}');
        	});
        	</script>
		}

		if @form
			html += %{
				<script>
				L(function(){
					
					$('.#{@form}-submit').on('click', function(){
						if($('form##{@form}').attr('method').toLowerCase() == "post")
							setTimeout(function(){
								table_#{@id}.ajax.reload( null, false );
							},650);
							
						if($('form##{@form}').attr('method').toLowerCase() == "put")
							table_#{@id}.row(table_#{@id}_last_index).data($('form##{@form}').serializeObject());
					});

				 	window.table_#{@id}.create = function(){
				 		$('form##{@form}').clearForm();
				 		$('form##{@form}').attr('method', 'POST');
				 		activate_form_#{@form}();
				 		$(document).trigger("create:form:opened", [ $('form##{@form}') ]);
				 		
					}

					window.table_#{@id}.edit = function(ev){
						var row = $(ev.target).closest('tr');
						var index = $("##{@id} > tbody > tr").index(row);
						window.table_#{@id}_last_index = index;
						var data = table_#{@id}.data()[index];
						$('form##{@form}').fillForm(data);
						$('form##{@form}').attr('method', 'PUT');
						activate_form_#{@form}();
						$(document).trigger("edit:form:opened", [ $('form##{@form}'), data ]);
					}

					window.table_#{@id}.delete = function(ev){
				 		var row = $(ev.target).closest('tr');
						var index = $("##{@id} > tbody > tr").index(row);
						var data = table_#{@id}.data()[index];
						delete_confirm(function(){
							$.ajax({
						        url: $('form##{@form}').attr('action'),
						        type: "DELETE",
						        data: data,
						        success: function(){
						        	Loader.hide();
						        	delete_success();
								    setTimeout(function(){window.location.reload(true)}, 500);
						        },
						        error: function(){
						        	Loader.hide();
						        	server_error();
						        },
							});
						});	
					}
				});
				</script>
			}
		end

		return html

	end

end