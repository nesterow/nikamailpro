=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "singleton"
require_relative "../config"

class Auth
	include Singleton

	# User methods are here
	class User

		def initialize(opts)
			@storage = Storage.new('webusers.storage')
			if opts
				@login = opts[:login]
				@opts = opts
				@user = get()
			end
		end

		def login
			@login
		end

		def exists
			return true if @storage.get(@login)
			return false
		end

		def find(opt)
			str = opt[:query]
            return @storage.all
		end

		def get
			user = @storage.get(@login)
			return user if user
			false
		end

		def create
			if exists()
				puts "[user db] User |#{@opts[:login]}| already exists"
				return false
			end
			begin
				data = hashPassword @opts
				@storage.add @login, data
				puts "[user db] Created new user |#{@opts[:login]}|"
				return true
			rescue Exception => e
				puts "[user db] Couldn't create new user |#{@opts[:login]}|"
				puts e.message
			end
		end

		def update(data)
			unless exists()
				puts "[user db] Cannot commit update. User |#{@opts[:login]}| does not exists."
				return false
			end
			begin
				puts "[user db] Updating user |#{@opts[:login]}|"
				password = @user[:password]
				password = hash_password(data[:password]) if data[:password]
				remove() if !data[:login].nil? && data[:login] != @opts[:login]
				data[:password] = password
				@login = @login || data[:login]
				@storage.add(@login, data)
				@user = @storage.get(@login)
				puts "[user db] User updated |#{@opts[:login]}|"
				return true
			rescue Exception => e
				puts "[user db] Error updating |#{@opts[:login]}|"
				puts e.message
			end
			
		end

		def remove
            STORAGE.remove(@login)
			true
		end

		def valid
			if @user
				u = get()
				return  check_password(@opts[:password], u[:password])
			else
				return false
			end
			return false
		end

		def hashPassword(opt)
			opt[:password] = hash_password(opt[:password])
			return opt
		end

		def checkPassword(hash)
            check_password(@opts[:password], hash)
		end



	end


	def user(opts = nil)
		return User.new(opts)
	end



end