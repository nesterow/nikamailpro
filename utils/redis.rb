=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "colorize"

class RedisServer

	def self.run
		@pid = nil
		@running = false
		@t = Thread.new {
			puts "Spawning redis server".yellow
			unless REDIS_CMD
				arch = `uname -m`[0..2]
				@pid = `lib/redis/bin/#{arch}/redis-server lib/redis.conf > /dev/null & echo $!`.strip
				puts "Redis server is runnig [#{@pid}]".green
			else
				puts "Spawning redis [#{REDIS_CMD}]".green
				@pid = `#{REDIS_CMD}`.strip
				puts "Redis server is runnig [#{@pid}]".green
			end
			
			@running = true
		}
		
	end

	def self.stop
		puts "Stopping redis [#{@pid}]".yellow
		system("kill -9 #{@pid}")
		@t.kill
		puts "Redis process stopped [#{@pid}]".green
	end

	def self.pid
		@pid
	end

	def self.running 
		@running
	end

end