=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "colorize"

class MongoServer

	def self.run
		@pid = nil
		@running = false
		@t = Thread.new {
			r = system(MONGODB_CMD)
			@pid = File.read('storage/db/mongod.lock').strip
			puts "Mongodb server is runnig [#{@pid}]".green
			@running = true
		}
	end
	
	def self.running
		@running
	end

	def self.stop
		system("kill #{@pid}")
		@t.kill
		puts "Mongodb process stopped [#{@pid}]".green
	end

end