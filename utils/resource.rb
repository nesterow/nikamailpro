=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

# This module provides basic rest api for a MongoDB 
# collection
#
# Usage:
# ```
#module Sinatra
#	module Todos
#		extend Resource
#		db Mongo::Client.new(MONGODB_URL)
#		name :todos
#		auth :post, :put, :delete
# 		....other settings...
#
#   end
#end
# ```
#
# Author:: Anton Nesterov (mailto:arch.nesterov@gmail.com)
# Copyright:: Copyright (c) 2018
# License:: BSD

require 'json'

module Resource

	#Http methods that need auth
	def auth(*http_methods)
		@auth = http_methods
	end

	#Turn off some CRUD operations
	def off(*http_methods)
		@off = http_methods
	end

	#Set access only for specified group
	def access(group, *http_methods)
		@access = @access || Hash.new
		http_methods.each { |m|  
			@access[m] = @access[m] || []
			@access[m].push(group)
		}
	end

	#Mongo client
	def db(obj)
		@db = obj
	end

	#Collenction name
	def name(name)
		@name = name
		@collection = @db[name]
	end

	# Allowed fields
	def fields(*args)
		@fields = args
	end

	# Fields that won't appear on get requests
	def protect(*args)
		@protect = args
	end

	def check_access(role, m)
		return true unless @access
		return @access[m].include? role
	end

	#Run blocks before CRUD method, passes params to the block
	def on_before(method, &block)
		@before_hooks = @before_hooks || Hash.new
		@before_hooks[method] = @before_hooks[method] || []
		@before_hooks[method].push block
	end

	#Run blocks after CRUD method, passes MongoDB result
	def on_after(method, &block)
		@after_hooks = @after_hooks || Hash.new
		@after_hooks[method] = @after_hooks[method] || []
		@after_hooks[method].push block
	end

	def handle_hooks(hooks = Hash.new, method, data)
		registry = hooks[method] || []
		registry.each { |cb|  
			cb.call(data)
		}
	end

	def admin bool
		@admin = bool
	end

	def get(route, &block)
		@get_methods = @get_methods || Hash.new
		@get_methods[route] = block
	end

	def post(route, &block)
		@post_methods = @post_methods || Hash.new
		@post_methods[route] = block
	end

	def put(route, &block)
		@put_methods = @put_methods || Hash.new
		@put_methods[route] = block
	end

	def delete(route, &block)
		@delete_methods = @delete_methods || Hash.new
		@delete_methods[route] = block
	end

	
	def registered(app)
		@off = @off || []
		auth = @auth || []
		admin = @admin || false
		collection = @collection
		protect = @protect || []
		fields = @fields

		indexes = Hash.new
		for f in fields
			indexes[f] = "text"
		end
		collection.indexes.create_one(indexes)


		@get_methods.each { |k,v|
			app.get(k, &v)
		} if @get_methods

		@post_methods.each { |k,v|
			app.post(k, &v)
		} if @post_methods

		@put_methods.each { |k,v|
			app.put(k, &v)
		} if @put_methods

		@delete_methods.each { |k,v|
			app.delete(k, &v)
		} if @delete_methods

		unless @off.include? :find
			app.get "/dbapi/#{@name}" do
				admin! if admin

				if auth.include? :find
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :find)
						return 401, ""
					end
				end


				opts = JSON.parse(params[:query] || '{}')
				limit = (params[:limit] || 50).to_i
				offset =  (params[:offset] || 0).to_i
				
				data = collection.find(opts).skip(offset).limit(limit)

				data = data.map {|e|
					if protect
						protect.each { |k|  
							e[k] = nil
						}
					end
				}

				return data.to_json

			end
		end

		unless @off.include? :table
			app.get "/dbapi/#{@name}/table" do
				admin! if admin

				if auth.include? :find
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :find)
						return 401, ""
					end
				end

				opts = Hash.new
				limit = (params[:length] || 50).to_i
				offset =  (params[:start] || 0).to_i

				search = params[:search]["value"]
				opts["$text"] = { "$search" => search} if search != ""
				
				data = collection.find(opts).skip(offset).limit(limit).to_a
				
				# res = data.map {|e|
				# 	if protect
				# 		protect.each { |k|  
				# 			e[k] = nil
				# 		}
				# 	end
				# }

				# puts res

				return {
					"recordsTotal" => data.length,
					"recordsFiltered" => data.length,
					:data => data
				}.to_json	

			end
		end

		unless @off.include? :get
			app.get "/dbapi/#{@name}/:id" do
				admin! if admin

				if auth.include? :get
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :get)
						return 401, ""
					end
				end


				id = params.delete(:id)
				data = collection.find({"_id"=>id}).first
				
				return 404, "Not found" unless data
				

				if protect
					protect.each { |k|  
						data[k] = nil
					}
				end
				return data.to_json

			end
		end

		unless @off.include? :post
			app.post "/dbapi/#{@name}" do
				admin! if admin

				if auth.include? :post
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :post)
						return 401, ""
					end
				end
				
				data = Hash.new
				for k in params.keys
					if fields.include? k.to_sym
						data[k] = params[k]
					end
				end

				data = collection.insert_one( data )
				
				return success! "Ok"

			end
		end

		unless @off.include? :put
			app.put "/dbapi/#{@name}/:id" do
				admin! if admin

				if auth.include? :put
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :put)
						return 401, ""
					end
				end
				
				
				data = Hash.new
				for k in params.keys
					if fields.include? k.to_sym
						data[k] = params[k]
					end
				end
				
				id = params.delete(:id)
				result = collection.update_one({"_id"=>BSON::ObjectId(id)}, {'$set' => data})

				return success! "Ok"
			end
		end

		unless @off.include? :put
			app.put "/dbapi/#{@name}" do
				admin! if admin

				if auth.include? :put
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :put)
						return 401, ""
					end
				end
								
				data = Hash.new
				for k in params.keys
					if fields.include? k.to_sym
						data[k] = params[k]
					end
				end
				
				id = params.delete(:id)
				result = collection.update_one({"_id"=>BSON::ObjectId(id)}, {'$set' => data})
				
				return success! "Ok"
			end
		end

		unless @off.include? :delete
			app.delete "/dbapi/#{@name}/:id" do
				admin! if admin

				if auth.include? :delete
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :delete)
						return 401, ""
					end
				end

				id = params["_id"]["$oid"] if params["_id"]
				data = collection.delete_one({"_id"=>BSON::ObjectId(id || params[:id])})

				return success! "Ok"
			end
		end

		unless @off.include? :delete
			app.delete "/dbapi/#{@name}" do
				admin! if admin

				if auth.include? :delete
					protected! 
					r = session[:user]["role"]
					unless check_access(r, :delete)
						return 401, ""
					end
				end
				id = params["_id"]["$oid"] if params["_id"]
				data = collection.delete_one({"_id"=> BSON::ObjectId(id || params[:id])})
				return success! "Ok"
			end
		end


	end


end