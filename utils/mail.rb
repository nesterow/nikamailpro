=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end


# This module is responsible for sending email notifications. 
# Modify it by adding new methods in order to provide 
# new notification types.
#
# == Usage:
# On controller level this class instance is provided by
# "email!" hook. In order to send an email you simply need 
# to call `email!(type).send(*args)`. 
# For example, `email!("confirm").send('test@local', 123)` would render 
# corresponding template and send email to 'test@local'
# 
# == Note:
# Use config variable 'TEST_EMAIL' to send all emails on developer's 
# inbox when on development environment.
#
# Author:: Anton Nesterov (mailto:arch.nesterov@gmail.com)
# Copyright:: Copyright (c) 2018
# License:: BSD

require_relative "../config"

class Mail
	
	# @t = render engine
	# @type = email type
	# @prod = bool environ
	def initialize(t, type, prod = false)
		@erb = t
		@prod = prod
		@type = type
	end

	def send(*args)
		case @type
		when "confirm"
			confirm(*args)	
		end
	end

	def confirm(email, token)
		email = TEST_EMAIL unless @prod
		email_body = @erb.call(:"email/confirm", {
			:locals=> {
				:action => "Confirm Email",
				:header => "Email Confirmation",
				:text => "One more step to complete registration",
				:href => "https://#{PUBLIC_HOST}/confirm/#{token}/confirm-email"
			}, 
			:layout=> :"email/layout"
		})
		Email.send(email, "Confirm Registration", email_body)
	end

end