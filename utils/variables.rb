=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "redis"
require "colorize"
require "json"
require "singleton"

class Variables
	
	include Singleton

	def initialize
		puts "[Variables] connecting to redis".yellow
		Thread.new {
			loop {
				break if connect()
			}
			puts "[Variables] connected to redis".green
		}
		
		
	end

	def all 
		all_keys = []
		loop {
	        cursor, keys = @redis.scan(cursor, :match => "key:*")
	        all_keys += keys
	        break if cursor == "0"
	    }
	    data = []
	    all_keys.each { |k| 
	    	d = Hash.new
	    	key = k.gsub("key:","")
	    	d[:name] = key
	    	d[:value] = @redis.get(k)
	    	data.push(d)
	    }
		return data
	end

	def get name
		@redis.get("key:#{name}")
	end

	def set name, value
		@redis.set("key:#{name}", value)
		@redis.save
	end

	def del name
		@redis.del("key:#{name}")
		@redis.save
	end

	def connect
		@hits = @hits || 1
		@hits += 1
		if @redis
			begin
				@redis.ping
				return @redis
			rescue Exception => e
				puts e.message.red if @hits > 105
				return false
			end
		end
		begin
			@redis = Redis.new(host: REDIS_HOST, port: REDIS_PORT, timeout:1, db: 5)
			@redis.ping
			return @redis
		rescue Exception => e
			puts e.message.red if @hits > 105
			return false
		end	
	end

end