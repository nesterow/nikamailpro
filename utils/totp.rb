=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "redis"
require "colorize"
require "json"
require "singleton"
require "rotp"

class TOTP
	
	include Singleton

	def initialize
		puts "[OTP] connecting to redis".yellow
		Thread.new {
			loop {
				break if connect()
			}
			puts "[OTP] connected to redis".green
		}
		
	end

	def all 
		all_keys = []
		loop {
	        cursor, keys = @redis.scan(cursor, :match => "otp:*")
	        all_keys += keys
	        break if cursor == "0"
	    }
	    data = []
	    all_keys.each { |k| 
	    	d = Hash.new
	    	key = k.gsub("otp:","")
	    	d[:name] = key
	    	d[:value] = @redis.get(k)
	    	data.push(d)
	    }
		return data
	end

	def get name
		@redis.get("otp:#{name}")
	end

	def set name
		value = ROTP::Base32.random_base32
		@redis.set("otp:#{name}", value)
		@redis.save
		return value
	end

	def del name
		@redis.del("otp:#{name}")
		@redis.save
	end

	def uri name, email
		totp = ROTP::TOTP.new(get(name), issuer: COMPANY_NAME)
		totp.provisioning_uri(email)
	end

	def verify name, code
		totp = ROTP::TOTP.new(get(name))
		totp.verify(code)
	end

	def connect
		@hits = @hits || 1
		@hits += 1
		if @redis
			begin
				@redis.ping
				return @redis
			rescue Exception => e
				puts e.message.red if @hits > 105
				return false
			end
		end
		begin
			@redis = Redis.new(host: REDIS_HOST, port: REDIS_PORT, timeout:1, db: 4)
			@redis.ping
			return @redis
		rescue Exception => e
			puts e.message.red if @hits > 105
			return false
		end	
	end


end