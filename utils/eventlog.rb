=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "redis"
require "colorize"
require "json"
require "singleton"

class EventLog
	
	include Singleton

	def initialize
		@handlers = []
		puts "[EventLog] connecting to redis".yellow
		Thread.new {
			loop {
				break if connect()
			}
			puts "[EventLog] connected to redis".green
		}
	end


	def publish(name, event)
		data = JSON.parse(redis.get(name) || '[]')
		data.push(event)
		redis.set(name, data.to_json)
		redis.expire(name, 3600 * 12)
	end

	def receive(name)
		d = redis.get(name)
		data = JSON.parse( d || '[]')
		redis.del(name) if d
		return data
	end

	def watcher(name, &block)
		@handlers.push({:name=> name, :proc=> block })
	end

	def redis
		@connection
	end

	def connect
		@hits = @hits || 1
		@hits += 1
		if @connection
			begin
				@connection.ping
				return @connection
			rescue Exception => e
				puts e.message.red if @hits > 105
				return false
			end
		end
		begin
			@connection = Redis.new(host: REDIS_HOST, port: REDIS_PORT, timeout: 1, db: 3)
			@connection.ping
			return @connection
		rescue Exception => e
			puts e.message.red if @hits > 105
			return false
		end	
	end

end