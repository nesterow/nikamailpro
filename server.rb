=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require_relative "./config"
require "sinatra/base"
require "sinatra/json"
require "sinatra/reloader" unless PRODUCTION
require "rack-session-file"

require_relative './utils/forms/sinatra_formkeeper'
require_relative "./routes/user"
require_relative "./routes/settings"
require_relative "./routes/boxes"
require_relative "./routes/groups"
require_relative "./routes/installer"
require_relative "./routes/test"

require 'keystores'
require 'webrick/https'

require "net/http"
require "socket"

class Server < Sinatra::Base
	
	
	use Rack::Session::File, :driver => :YAML
	set :environment, :production if PRODUCTION
	set :root, File.dirname(__FILE__)
	set :public_folder, Proc.new { File.join(root, "public") }
	enable :logging
	

	configure :development do
		register Sinatra::Reloader
 	end


 	register Sinatra::FormKeeper
 	register Sinatra::User
 	register Sinatra::Settings
 	register Sinatra::Boxes
 	register Sinatra::Groups
 	register Sinatra::Installer
 	#register Sinatra::AppTest
 	
 	module Helpers

 		def url u
 			unless u.end_with? "/"
 				return "#{u}"
 			else
 				return u
 			end
 		end

 		def locale
 			if File.exist? confile('locale')
				return File.read confile('locale')
			else
				return "en"
			end
 		end

 		def lang
 			return Lang.msg(locale)
 		end
 		

 	end

 	helpers Helpers
 	
 	
 	
 	def restart_container data
		id = data['Id']
		sock = Net::BufferedIO.new(UNIXSocket.new("/var/run/docker.sock"))
		request = Net::HTTP::Post.new("/v1.24/containers/#{id}/restart")
		request['Host'] = 'localhost'
		request.exec(sock, "1.1", "/v1.24/containers/#{id}/restart")
		begin
		  response = Net::HTTPResponse.read_new(sock)
		end while response.kind_of?(Net::HTTPContinue)
		response.reading_body(sock, request.response_body_permitted?) { }
		puts response.body
	end
 	
 	before do
		unless is_installed?
			redirect '/install' unless request.url.include? '/install'
		end
	end
 	

 	get '/' do
		protected!
 		return redirect(url('/boxes'))
 	end
 	
 	get '/status' do
		success! "OK"
	end
 	
 	get '/docs/' do
		protected!
		File.read(File.join(File.dirname(__FILE__), 'public/docs/index.html'))
	end
 	
 	get '/restart' do
		protected!
		erb :"manage/finish", locals:{
          section_title: "RESTART SERVER",
        }
	end
 	
 	post '/reboot' do
		protected!
		
		if File.exist? confile('supervisor.conf',false)
			`supervisorctl restart all`
			return success! "OK"
		end
		
		sock = Net::BufferedIO.new(UNIXSocket.new("/var/run/docker.sock"))
		request = Net::HTTP::Get.new("/v1.24/containers/json")
		request['Host'] = 'localhost'
		request.exec(sock, "1.1", "/v1.24/containers/json")
		
		begin
		  response = Net::HTTPResponse.read_new(sock)
		end while response.kind_of?(Net::HTTPContinue)
		response.reading_body(sock, request.response_body_permitted?) { }
		data = ::JSON.parse(response.body)
		containers = data.select {|e|
			e['Image'] == 'nikamail:latest'
		}
		containers.each { |e|
			restart_container(e)
		}
		return success! "OK"
	end
 	
 	


	def self.run!
		keystore = OpenSSL::JKS.new
		keystore.load(confile(KEYSTORE), KEYSTORE_PASSWORD)
		cert = keystore.get_certificate(KEYSTORE_ALIAS)
		key = keystore.get_key(KEYSTORE_ALIAS, KEYSTORE_PASSWORD)
		server_options = {
		  :Host => HTTP_HOST,
		  :Port => HTTP_PORT,
		  :SSLEnable => true,
		  :SSLCertificate => OpenSSL::X509::Certificate.new(cert),
		  :SSLPrivateKey => OpenSSL::PKey::RSA.new(key),
		  :SSLCertName => [ [ "CN",WEBrick::Utils::getservername ] ]
		}
		Rack::Handler::WEBrick.run self, server_options
    end
 	
end
