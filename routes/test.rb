=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require 'sinatra/base'
require 'sinatra/json'

require_relative '../utils/forms/mdl'
require_relative '../utils/views/table'

class Form < MDLForm
	action "/"
	method "POST"

  select(
    option("Red", :red),
    option("Green", :green),
    option("Blue", :blue),
    :title => "Colour",
    :name => :color
  )

	input "Some", :type => :text, :name => :one, :required => true, :error => "Wrong"
	input "Another", :name=> :two, :type => :text, :pattern => '^[0-9]$', :error => "Wrong input"
	textarea "Bla-bla bla", :name => "textarea"

  checkbox "Check Me", :name => :check

  radio(
    option("Yes", 1, true),
    option("No", 2),
    option("Not Sure", 3),
    :name => :options,
    :title => "Are you sure?"
  )



  switch "Turn on teapot?", :checked=>true, :name=>:teapot
end


class Table < DataTable
  id :example
  api "/manage/users/table"
  col :login, "Login"
  col :email, "Email"
  col :role, "Role", "user"
  button "Add User", "add_user"
  button "Alert", "alert"
  action :edit, "edit_user"
  action :remove, "remove_user"
end

class DrawerForm < MDLForm
  id "drawer"
  drawer true
  radio(
    option("Yes", 1, true),
    option("No", 2),
    option("Not Sure", 3),
    :name => :options,
    :title => "Are you sure?"
  )
  input "Some", :type => :text, :name => :one, :required => true, :error => "Wrong"
  input "Another", :name=> :two, :type => :text, :pattern => '^[0-9]$', :error => "Wrong input"
end

module Sinatra
  module AppTest
  	def self.registered(app)

  		app.get "/_form_test" do
  			erb :"test/form", :locals => {
  				:form => Form,
          :drawer => DrawerForm
  			}
  		end

      app.get "/_table_test" do
        erb :"test/table", :locals =>{
          :tables => true,
          :table => Table
        }
      end

  	end
  end
end