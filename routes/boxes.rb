=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

module Sinatra
  module Boxes
    
    def self.registered(app)
      
      app.get '/boxes' do
        
        protected!
        
        erb :boxes, locals: {
          section_title: "MANAGE EMAIL BOXES",
          riot: [
            'tags/users',
            'tags/box'
          ]
        }
      end
      
      app.get '/addresses' do
        protected!
        query = params[:filter]
        boxes = Storage.new('users.storage').all.select {|b|
          (
            !b.include?('subscription-') &&
            !b.include?('privategroup-') &&
            !b.include?('opengroup-')
          )
        }
        aliases = Storage.new('aliases.storage').all
        forwarding = Storage.new('forwarding.storage').all
        result = []
        
        boxes.keys.each { |key|
          uidf = storagefile("maildrops/#{key}/uid.txt", false)
          if File.exist?(uidf)
            uid = File.read(uidf)
          else
            uid = false
          end
          desc = Hash.new
          desc[:address] = key
          desc[:domain] = DOMAIN
          desc[:aliases] = aliases[key]
          desc[:forwarding] = forwarding[key]
          desc[:count] = uid
          result.push(desc)
        }
        result = result.select {|e|
          e[:address].include? query
        }
        return result[0..25].to_json
      end
      
      app.post '/boxes' do
        protected!
        name = params[:name] || ''
        password = params[:password] || ''
        updating = params[:updating]
        
        if name.length < 1 || password.length < 4
          return fail! "Invalid Input", 400
        end
        
        if updating == 'true' && password.length > 3
          begin
            Mireka.removeUser(name)
            Mireka.addUser(username: name, password: password)
          rescue
            return fail! "Mailbox Does Not Exist", 400
          end
        else
          begin
            Mireka.addUser(username: name, password: password)
          rescue
            return fail! "Mailbox Already Exists", 400
          end
        end
        {}.to_json
      end
      
      app.post '/boxes/delete' do
        protected!
        name = params[:name] || ''

        if name.length < 1 
          return fail! "Invalid Input", 400
        end
        
        begin
          Mireka.removeUser(name)
        rescue
          return fail! "Mailbox Does Not Exist", 400
        end
        
        {}.to_json
      end
      
    end
  end
end