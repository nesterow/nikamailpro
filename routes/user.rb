=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require 'sinatra/base'
require 'sinatra/json'

require_relative '../utils/simpleauth'

module Sinatra
  module User

    module Helpers
      def authorized?
        session[:authorized]
      end

      def protected!
        redirect '/login' unless authorized?
      end

      def admin?
        is! "admin"
      end

      def admin!
        redirect '/' unless is! "admin"
      end

      def is!(who)
      	if session[:authorized]
      		if session[:user]["role"].nil?
      			return false
      		end
      		return session[:user]["role"] == who
      	end
      	false
      end

      def logout!
      	session[:notice] = false
        session[:authorized] = false
    	  session[:user] = {}
      end

      def success!(message=nil)
      	return 200, json(:message=>message)
      end

      def fail!(message=nil, status=400)
      	return status, json(:message=>message)
      end

      def message!(message)
      	return erb :"misc/message", :locals => {:message => message}
      end

    end

    def self.registered(app)
    	app.helpers User::Helpers

    	auth = Auth.instance

    	
    	app.get '/login' do
    		return redirect("/") if authorized?
    		erb :login, :locals => {
          :section_title => "LOGIN",
          :form => lang.forms.login
        }
    	end

    	app.post '/login' do
    		form do
          field :username, :present => true, :length => 5..10
          field :password, :present => true, :length => 5..24
        end
        if form.failed?
          return fail! lang.user.form_fail, 401
    		else
    			user = auth.user(
    				:login => form[:username], 
    				:password => form[:password]
    			)
    			unless user.valid
    				return fail! lang.user.wrong_password, 401
    			end
    			data = user.get()
    			data[:login] = form[:username];
    			session[:authorized] = true
    			session[:user] = data
    			return success! lang.user.login_success
    		end
    	end

    	app.get '/logout' do
    		logout!
    		redirect "/"
    	end
      
      
    	app.get '/signup' do
        return redirect("/")
    	
    		erb :signup, :locals => {
          :form => lang.forms.registration
        }
    	end

    	app.post '/signup' do
    		return redirect("/")
    	
    		form do
    			field :email, :present => true, :email => true, :bytesize => 10..255
          field :username, :present => true, :length => 5..10
          field :password, :present => true, :length => 5..24
        end
    		
        if form.failed?
          return fail! lang.user.form_fail, 400
    		else

    			created = auth.user(
    				:login => form[:username], 
    				:password => form[:password],
    				:email => form[:email]
    			).create()

    			unless created
    				return fail! (lang.user.duplicate_username % [form[:username]]), 401
    			end

    			return success! lang.user.register_success
    		end
        
    	end

    end
    
  end
end