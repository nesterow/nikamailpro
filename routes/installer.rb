=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

module Sinatra
  module Installer
    
    module Helpers
      def is_installed?
        File.exist? confile('.install_lock', false)
      end
    end
    
    def self.registered app
      app.helpers Installer::Helpers
      auth = Auth.instance
      
      app.get "/install" do
        redirect '/' if is_installed?
        erb :"manage/install", locals:{
          section_title: "CONFIGURE NIKAMAIL",
          form: PreconfigForm
        }
      end
  
      app.get '/install/settings' do
        redirect '/' if is_installed?
        erb :"manage/install", locals:{
          section_title: "SERVER SETTINGS",
          form: ServerPreconfigForm
        }
      end
      
      app.get '/install/finish' do
        redirect '/' if is_installed?
        FileUtils.touch(confile('.install_lock', false))
        erb :"manage/finish", locals:{
          section_title: "REBOOT SERVER",
        }
      end
      
      app.post "/install" do
        halt 400 if is_installed?
        case params[:action]
        when 'general'
          language = params[:language] || 'en'
          session[:lang] = language
          form do
            field :domain, :present => true, :length => 5..24
            field :username, :present => true, :length => 5..10
            field :password, :present => true, :length => 5..24
          end
          return fail!(lang.user.form_fail, 401) if form.failed?
          
          domain = form[:domain]
          username = form[:username]
          password = form[:password]
          
          #save language
          open(confile('locale'), 'w') { |f|
            f.write(language)
            f.close()
          }
          
          open(confile('domain.fqdn'), 'w') { |f|
            f.write(domain)
            f.close()
          }
          
          user_storage = Storage.new('webusers.storage')
          user_storage.clean()
          
          created = auth.user(
              :login => form[:username], 
              :password => form[:password],
              :lang => form[:language]
          ).create()
          
          return success! 'OK'
        
        when 'server'
          mta = params[:mta] && 'true' || 'false'
          mx = params[:mx] && 'true' || 'false'
          pop = params[:pop] && 'true' || 'false'
          data = "MTA_SERVER_ON=#{mta}\nMX_SERVER_ON=#{mx}\nPOP3_SERVER_ON=#{pop}\n"
          open(confile('server.rb'), 'w') { |f|
            f.write(data)
            f.close()
          }
          return success! 'OK'
        end
        return fail! 'Wat?', 400
      end
      
    end
    
  end
end