=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

module Sinatra
  module Groups
    
    module Listing
      
      def self.load box
        data = File.read(storagefile("lists/#{box}.list"))
        data.length != 0 && Marshal.load(data) || Hash.new
      end
      
      def self.save data
        email = data[:mailbox]
        dump = Marshal.dump(data)
        file = File.open storagefile("lists/#{email}.list"), 'w'
        file.write(dump)
        file.close
      end
      
      def self.headers
        path = storagedir('lists')
        result = []
        begin
          Dir["#{path}/*.list"].each {|file|
            content = File.read(file)
            data = Marshal.load(content)
            result.push({
              name: data[:name],
              mailbox: data[:mailbox],
              relay: data[:relay],
              type: data[:type],
              length: data[:list].length
            })
          }
        rescue
        end
        return result
      end
      
    end
    
    def self.registered(app)
      root = storagedir('lists')
      
      app.get '/groups' do
        protected!
        erb :groups, locals: {
          section_title: lang.lists.title,
          riot: [
            'tags/listings',
            'tags/group'
          ]
        }
      end
      
      app.get '/group/lists' do
        protected!
        Listing.headers.to_json
      end
      
      app.get '/group/list/:box' do
        protected!
        box = params[:box]
        if File.exist? storagefile("lists/#{box}.list")
          return Listing.load(box).to_json
        else
          return ""
        end
      end
      
      app.delete '/group/list/:box' do
        protected!
        box = params[:box]
        if File.exist? storagefile("lists/#{box}.list")
          File.delete storagefile("lists/#{box}.list")
          Mireka.removeUser(box);
        else
          halt 404
        end
        ""
      end
      
      app.post '/group/list/:box' do
        protected!
        create = params[:create]
        name = params[:name]
        box = params[:box]
        list = params[:list] || '[]'
        type = params[:type]
        sender = params[:sender]
        
        if create
          salt = (0..6).map { ('a'..'z').to_a[rand(26)] }.join
          password = (0..12).map { ('a'..'z').to_a[rand(26)] }.join
          relay = "#{type}-#{salt}"
          Mireka.addUser(username: relay, password: password);
          Mireka.addUser(username: box, password: password);
          
          Listing.save({
            name: name,
            mailbox: box,
            password: password,
            type: type,
            relay: relay,
            sender: sender,
            list: ::JSON.parse(list),
          })
          
        else
          settings = Listing.load(box)
          settings[:list] = ::JSON.parse(list)
          password = params[:password]
          if password
            Mireka.removeUser(settings[:relay]);
            Mireka.removeUser(settings[:mailbox]);
            Mireka.addUser(username: settings[:relay], password: password);
            Mireka.addUser(username: settings[:mailbox], password: password);
            settings[:password] = password
          end
          settings[:sender] = sender
          Listing.save(settings);
        end
        
        Hooks.load_extensions()
        
        {}.to_json
      end
      
      
      
      app.get '/api/:box' do
          box = params[:box]
          unless File.exist? "#{storagedir("lists")}/#{box}.list"
            halt 404
          end
          settings = Listing.load(box)
          halt 401 if params[:password] != settings[:password]
          return settings[:list].to_json
      end
      
      app.post '/api/:box' do
          box = params[:box]
          unless File.exist? "#{storagedir("lists")}/#{box}.list"
            halt 404
          end
          settings = Listing.load(box)
          halt(401) if params[:password] != settings[:password]
          
          request.body.rewind
          data = ::JSON.parse(request.body.read)
          halt(400) unless data['address']
          
          settings[:list].push({
            'address' => data['address']
          })
          Listing.save(settings)
          Hooks.load_extensions()
          {}.to_json
      end
      
      app.delete '/api/:box' do
          box = params[:box]
          unless File.exist? "#{storagedir("lists")}/#{box}.list"
            halt 404
          end
          settings = Listing.load(box)
          halt(401) if params[:password] != settings[:password]
          
          request.body.rewind
          data = ::JSON.parse(request.body.read)
          halt(400) unless data['address']
          
          
          settings[:list] = settings[:list].select {|a|
            a['address'] != data['address']
          }
          
          Listing.save(settings)
          Hooks.load_extensions()
          {}.to_json
          
      end
      
      
    end
  end
end