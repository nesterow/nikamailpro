=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

module Sinatra
	module Settings

		def self.registered app
			auth = Auth.instance
			app.get '/settings' do
				protected!
				settings = {
					language: locale,
					username: session[:user][:login],
					domain: File.read(confile('domain.fqdn')),
					pop: POP3_SERVER_ON,
					mx: MX_SERVER_ON,
					mta: MTA_SERVER_ON,
					dkim: DKIM_ON
				}
				
				erb :settings, :locals => {
					section_title: lang.settings.title, 
					settings: settings.to_json,
					form: lang.forms.settings
				}
			end

			app.post '/settings' do
				protected!
				
				form do
					field :domain, :present => true, :length => 5..24
					field :password, :present => true, :length => 5..24
				end
				
				return fail!(lang.password_required, 400) if form.failed?

				user = auth.user(
    				:login => session[:user][:login], 
    				:password => form[:password]
    			)
    			unless user.valid
    				return fail! lang.user.wrong_password, 401
    			end
				
				#save language
				open(confile('locale'), 'w') { |f|
					f.write(params[:language])
					f.close()
				}
				
				#save domain
				open(confile('domain.fqdn'), 'w') { |f|
					f.write(params[:domain])
					f.close()
				}
				
				#save new password
				new_password = params[:new_password].to_s.strip
				if new_password != ''
					if new_password != params[:repeat_new_password].strip
						return fail! lang.password_dont_match, 400
					end
					auth.user(:login => session[:user][:login]).update({
						:password => new_password
					})
				end
				
				#save server settings
				mta = params[:mta] && 'true' || 'false'
				mx = params[:mx] && 'true' || 'false'
				pop = params[:pop] && 'true' || 'false'
				data = "MTA_SERVER_ON=#{mta}\nMX_SERVER_ON=#{mx}\nPOP3_SERVER_ON=#{pop}\n"
				open(confile('server.rb'), 'w') { |f|
				  f.write(data)
				  f.close()
				}
				
				#rewrite certs
				keystore = params[:keystore_raw]
				keystore_pwd = params[:keystore_password]
				keystore_alias = params[:keystore_alias]
				if keystore.to_s != '' && (
					keystore_pwd.to_s == '' || keystore_alias.to_s == '')
					return fail! lang.keystore_pass_required, 400
				end
				if keystore.to_s != '' && keystore_pwd.to_s != '' && keystore_alias.to_s != ''
					base = keystore[keystore.index('base64,') + 7 .. -1]
					bin = Base64.decode64(base)
					name = 'updated.jks'
					File.open(confile(name), 'wb') { |f| f.write(bin) }
					
					#check if keystore is valid
					begin
						ks = OpenSSL::JKS.new
						ks.load(confile(name), keystore_pwd)
						ks.get_certificate(keystore_alias)
					rescue
						return fail! "Keystore is invald", 400
					end
					
					data = %{KEYSTORE_PASSWORD="#{keystore_pwd}"\nKEYSTORE="#{name}"\nKEYSTORE_ALIAS="#{keystore_alias}"}
					open(confile('keystore.rb'), 'w') { |f| f.write(data) }
				end
				
				#DKIM
				dkim = params[:dkim]
				dkim_key = params[:dkim_key_raw]

                unless dkim
                    dkim_data = %{DKIM_ON=false\nDKIM_PRIVATE_KEY=File.read(confile('dkim.key'));}
					open(confile('dkim.rb'), 'w') { |f|
						f.write(dkim_data)
						f.close()
					}
                end
                
				
                
				
                if dkim && dkim_key
					base = dkim_key[dkim_key.index('base64,') + 7 .. -1]
					bin = Base64.decode64(base)
					unless bin.include? "-----BEGIN RSA PRIVATE KEY-----"
                        return fail! "DKIM: Attached file isn't RSA private key", 400
					end
					File.open(confile('dkim.key'), 'wb') {|f| f.write(bin) }
                    
                    dkim_data = %{DKIM_ON=true\nDKIM_PRIVATE_KEY=File.read(confile('dkim.key'));}
                    open(confile('dkim.rb'), 'w') { |f|
					 f.write(dkim_data)
					 f.close()
					}
                end
                
				
				success! "OK"
			end

		end
	end
end