=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require 'json'
require 'sinatra/base'
require_relative '../../utils/auth'

module Sinatra
	module UserMgr
		
		def self.registered(app)
			auth = Auth.instance

			app.get "/manage/users" do
				admin!
				erb :"manage/users", :locals => {
					:tables => true,
					:userform => lang.forms.users,
					:table => lang.forms.users_table
				}
			end

			app.get "/manage/users/table" do
				admin!

				length = params[:length].to_i
				start = params[:start].to_i
				search = params[:search]["value"]

				keys = auth.user.find :query => search
				data = keys[start...start+length]
				
				res = []
				data.each { |k| 
					res.push(::JSON.parse(auth.redis.get(k) || '{}'))
				}

				return {
					"recordsTotal" => keys.length,
					"recordsFiltered" => keys.length,
					:data => res
				}.to_json	
			
			end

			app.post "/manage/users" do
				admin!

				form do
    				field :email, :present => true, :email => true, :bytesize => 10..255
					field :login, :present => true, :length => 5..10
					field :password, :present => true, :length => 5..24
					field :role, :present => true
				end

				if form.failed?
					return fail! lang.user.create_form_fail, 400
				end

				email = auth.user.saveEmail form[:email]
    			unless email
    				return fail! lang.user.duplicate_email % [form[:email]], 401
    			end

				created = auth.user(
    				:login => form[:login], 
    				:password => form[:password],
    				:email => form[:email],
    				:role => form[:role]
    			).create()

    			unless created
    				auth.user.removeEmail(email)
    				return fail! lang.user.duplicate_username % [form[:login]], 401
    			end

    			token = auth.user.generateToken
    			auth.user.setTokenData(token, form[:email])
    			email!('confirm').send(email, token) unless EMAIL_REQUIRED_CONFIRMATION
    			return success! lang.user.created
			end

			app.put "/manage/users" do
				admin!

				data = Hash.new
				data[:role] = params[:role] if params[:role]
				data[:login] = params[:login] if params[:login]
				data[:email] = params[:email] if params[:email]
				data[:password] = params[:password] if params[:password] && params[:password] != ""
				begin
					auth.user(:login=>params[:id]).update(data)
				rescue
					return fail! "Error", 400
				end
				return success! lang.user.updated
			end
			
			app.delete "/manage/users" do
				admin!

				auth.user(:login=>params[:login]).remove()
				return success! lang.user.deleted
			end


		end
		

	end
end
