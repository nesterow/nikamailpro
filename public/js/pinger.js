!function(){

window.pinger = window.pinger || {};

var INTRERVAL = 1500;
function handle(data){
	if(data.auth)
		INTRERVAL = 1500;
	else
		INTRERVAL = 5000;
	if(data)
	JSON.parse(data).events.forEach(function(ev){
		if(pinger[ev.type])pinger[ev.type](ev);
	});
}

function ping(){
  $.get('/ping')
	.then(function(data){
		handle(data);
		setTimeout(ping, INTRERVAL);
	})
	.fail(function(err){
		console.log(err);
		setTimeout(ping, INTRERVAL * 3);
	});

};
ping();


}();