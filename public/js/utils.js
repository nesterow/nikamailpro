
jQuery.fn.extend({
	submitForm : function () {
		var that = this;
		var valid;
		var message = "";
		Loader.show();
		this.find('input,textarea').each(function(){
			if(!valid) valid = $(this).is(":valid");
		})
		if(!valid)
			return jQuery.Deferred().promise();
		return $.ajax({
	        url: that.attr("action"),
	        type: that.attr("method"),
	        data: that.serialize(),
	        success: Loader.hide,
	        error: Loader.hide,
	    })
	},
	fillForm : function(data){
		var that = this;
		$(that).find('input,select').each(function(){
			if(this.type === 'password') return;
			if(this.type === 'checkbox'){	
				this.checked = !!data[this.name];
				if(this.checked) $(this).parent().addClass('is-checked');
			}
			else{
				this.value = data[this.name] || "";
			}
			$(this).parent().find("*[data-val='"+data[this.name]+"']").click();
			this.dispatchEvent(new Event('input'));
		});
		$(that).find('textarea').each(function(){
			this.innerHTML = data[this.name] || "";
			this.dispatchEvent(new Event('input'));
		});
	},
	clearForm : function(){
		var that = this;
		$(that).find('input,select').each(function(){
			this.value = "";
			$(this).parent().find("*[data-val]").first().click();
			this.dispatchEvent(new Event('input'));
		});
		$(that).find('textarea').each(function(){
			this.innerHTML = "";
			this.dispatchEvent(new Event('input'));
		});
	},
	serializeObject:function(){
		var array = $(this).serializeArray();
		var obj = {};
		for (var i = array.length - 1; i >= 0; i--) {
			var o = array[i];
			obj[o.name] = o.value;
		}
		return obj
	},
});

function Error(message, head){
	var dialog = document.querySelector('dialog');
	$(dialog).find('#title').html(head);
	$(dialog).find('#title').addClass("red");
	$(dialog).find("#error").html(message);
	dialog.showModal();
	$(dialog).one('click', dialog.close);
}

function ResponseError(res){
	var dialog = document.querySelector('dialog');
	// $(dialog).find('#title').html("&times;");
	$(dialog).find('#title').addClass("red");
	$(dialog).find("#error").html((
		res.responseJSON && 
		res.responseJSON.message || 
	"Server Error #"+res.status).replace(/\n/g, '<br>'));
	try{
		dialog.showModal();
	}catch(e){

	}
	
	$(dialog).one('click', dialog.close);
}

function Warning(message, head){
	var dialog = document.querySelector('dialog');
	$(dialog).find('#title').html(head);
	$(dialog).find("#message").html(message);
	dialog.showModal();
	$(dialog).one('click', dialog.close);
}

function Toast(message, callback){
	var snackbarContainer = document.querySelector('#toast');
	snackbarContainer.MaterialSnackbar.showSnackbar({
		message: message,
		actionHandler: callback,
		timeout: 3200,
		actionText: "🔳➡️"
	});
}

function Notify(data){
	var snackbarContainer = document.querySelector('#toast');
	snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

var Loader = {
	show: function(){
		$('#loader').show();
	},
	hide: function(){
		$('#loader').hide();
	}
};
window.onbeforeunload = function() {
  Loader.show();
};
$(window).on("load", Loader.hide);


$('.notice .close').click(function(){
	$('.notice').remove();
});


function InitForm(id, redirect){
	var el = document.getElementById(id);
	var btn = $('.'+id).find('.'+id+"-submit");
	$(el).find('.required').focusout(function(){
		$(this).attr('required', 'true');
		this.dispatchEvent(new Event('input'));
	});
	$(el).find('input').not('input[type=checkbox]')
	.last().on("keypress",function(ev){
		if(ev.keyCode == 13)
			btn.trigger('click');
	})
	btn.on('click', function(){
		$(el).submitForm()
			.then(Notify)
			.then(function(){
				if(!redirect)return;
				if(redirect!="none"){
					window.location.href = redirect;
				}
			})
			.fail(ResponseError);
	})
}

function outsideDialog(event) {
  var dialog = this;
  var rect = dialog.getBoundingClientRect();
  var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
      && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
  if (!isInDialog) {
    dialog.close();
    dialog.removeEventListener('click', outsideDialog);
		return true;
  }
}

///-------------------------------------------------

function Reboot(redirect) {
	$.post('/reboot');
	var loading = swal({
		title: 'Rebooting!',
		text: 'Please wait a moment..',
		backdrop: false,
		onOpen: () => {
			swal.showLoading();
		}
	}).then((result) => {
		if (
			result.dismiss === 'reboot'
		) {
			console.log('I was closed by the timer');
		}
	});
	var attempts = 0;
	var started = false;
	var finished = false;
	function check_server(){
		$.get('/status')
		.then(function(){
			if(started){
				finished = true;
				swal.close();
				swal("Server was restarted!", 'success');
				if(redirect)setTimeout(function(){
					window.location.href = redirect;
				},1000);
			}
		})
		.fail(function(){
			started = true;
			console.log("Waiting for reboot #"+ attempts);
			attempts++;
		})
		
		if(!finished)
			return setTimeout(check_server,400);
	}
	check_server();
	
};
