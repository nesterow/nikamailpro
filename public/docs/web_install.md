INITIAL CONFIGURATION
---------------------

After NikaMail is running on your system visit `https://{SERVER_IP_ADDRESS}:10080`
in order to access web console and proceed with configuration.

#### 1) Add exception for self-signed certificate
WebUI uses a self-signed certificate in order to provide a secure conection.
It uses the same cerificate for STARTTLS and WebUI.
Later you can replace it with your own cert, but at start you have to add exception in your browser:

![Screenshot1](images/web_cert_exception.png)

#### 2) Domain name & Admin password
NikaMail must know what domain name it accepts emails for. The domain name should be a
Fully Qualified Domain Name i.e. `example.com`. However if you use it for local delivery,
internal domains will also work as long as your DNS server includes related records.


Also, you need to provide admin username and password which will later be used to access admin panel.

![Screenshot2](images/web_domain.png)


#### 3) Turn on Server Features
At last you need to turn on needed server features.
For example if you only need MTA select only corresponding option.
![Screenshot3](images/web_server.png)

#### 4) Add first mailbox
After inital configuration is complete you need to add a mailbox in order to receive/send emails.

Press "add user" button and input username and password.
![Screenshot4](images/web_adduser.png)

#### 5) Configure DNS
If you use a public domain you need to add a SPF record to DNS configuration,
otherwise most email servers won't accept messages from your server.
![Screenshot5](images/web_dns.png)
