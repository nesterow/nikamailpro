Commands
--------
```
    make build         - (re)build docker container and java extensions
    make console       - run ineractive console
    make run           - run NikaMail
    make start         - run NikaMail in daemon mode (docker)
    make stop          - stop daemon
    make restart       - restart daemon
```

Using command line
------------------
NikaMail command line tool is simply a Ruby IRB. That means you should use all commands as you would use ruby methods.

Start console
```
    ~# make console                      (with docker)
    ~# bin/jruby app/main.rb console     (w/o docker)
```

Example:
```ruby

    (irb)> adduser 'mario', 'password123'  # add a new user
    (irb)> adduser 'luiji', 'password123'
    (irb)> setalias 'mario+castle@domain.fqdn', 'mario@domain.fqdn' # set alias
    (irb)> forward 'bros@domain.fqdn', 'mario@domain.fqdn', 'luiji@domain.fqdn'

```