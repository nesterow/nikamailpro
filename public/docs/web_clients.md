CONFIGURE AN EMAIL CLIENT
-------------------------

Currently **NikaMail** only supports POP3 for accessing received emails.
You should use an email client which supports spam filtering or you can pair it with GMail or other services.


### Adding an Account
* Specify your mail server domain.
* Select POP3 as main protocol
* Input mailbox name and password


![Screenshot](images/web_pop3.png)

### Configuring SMTP
* Turn on SMTP authentication
* Input mailbox name and password


![Screenshot](images/web_smtp.png)

### Using StartTLS
* Select StartTLS as encryption method for both POP3 and SMTP


![Screenshot](images/web_starttls.png)

### Setting Ports
* Set 587 as SMTP port
* Set 110 as POP3 port


![Screensho3](images/web_ports.png)






