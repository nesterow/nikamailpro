Configuration files
-------------------
All configuration files are stored in `config` directory
```
config
├── configuration.rb    -- main configuration
├── domain.fqdn         -- domain name
├── host.list           -- additional hosts
├── keystore.jks        -- certificate keystore
├── keystore.rb         -- keystore configuration
├── logback.xml         -- mireka logging configuration
├── server.rb           -- server configuration
```

#### SET DOMAIN NAME
Currently server accepts emails only per user, it doesn't provide mailboxes for multiple domains.
However you can alias as many hostnames as you want.

Open `domain.fqdn` and write a Fully Qualified Domain Name. All registered mailboxes are related to this domain.
If need to alias additional domains edit `host.list`.

#### SETUP SERVER FUNCTIONALITY
By default server lauches with full functionality supporting MX on port 25, MTA on port 587, and POP3 on port 110.
If you don't need all functionality edit `server.rb` and restart application.
```ruby
MTA_SERVER_ON=true
MX_SERVER_ON=true
POP3_SERVER_ON=true
```

#### RUBY METHODS
NikaMail has certain helper methods available in global scope. You can use them in configuration files
```ruby
    confile('filename')               -- returns full path to a configuration file
    javaconf('property', 'value')     -- sets system properties for JVM
    storagefile('filename')           -- return full path for a storage file
```

