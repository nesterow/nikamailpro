Installing On Raspberry PI
-----------------------
##### Warning!! SD cards aren't designed to perform large number of read/write operations with thousands of small files. If your email server expected to handle hundreds of emails per day you should consider to use a usb drive to keep NikaMail

1. Unpack NikaMail distribution to a usb SSD/HARD drive
2. Connect your drive to the PI device 
3. SSH to your device and execute following commands:
```
    ~# cd /path/to/nikamail
    ~# chmod u+x ./bin/*
    ~# sudo bin/install.sh
```
After installation visit `https://{Raspi IP}:10080` in order to finish configuration