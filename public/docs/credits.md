CREDITS
---------------
#### Cool software built by thousands awesome people  

-----------------------------------------------------------
>### Mireka (Java)
[Hontvári Levente](https://github.com/hontvari)<br>
<small>[Mireka Documentation](http://mireka.org/)</small><br>
<small>[Github](https://github.com/hontvari/mireka)</small>

-----------------------------------------------------------
>### SubEtha (Java)
[Jeff Schnitzer](https://github.com/stickfigure)<br>
[Jon Stevens](https://github.com/lookfirst)<br>
[Scott Hernandez](https://github.com/scotthernandez)<br>
[All Contributors](https://github.com/voodoodyne/subetha/graphs/contributors)<br>
<small>[Github](https://github.com/voodoodyne/subetha)</small>

-----------------------------------------------------------
>### JRuby (Java)
[JRuby Team](https://github.com/orgs/jruby/people)<br>
[All Contributors](https://github.com/jruby/jruby/graphs/contributors)<br>
<small>[Github](https://github.com/jruby/jruby)</small>

-----------------------------------------------------------
>### Pure Ruby DKIM (Ruby)
[John Hawthorn](https://github.com/jhawthorn)<br>
[All Contributors](https://github.com/jhawthorn/dkim/graphs/contributors)<br>
<small>[Github](https://github.com/jhawthorn/dkim)</small>

-----------------------------------------------------------
>### Mini Smtp Server (Ruby)
[Aaron Gough](https://github.com/aarongough)<br>
[All Contributors](https://github.com/aarongough/mini-smtp-server/graphs/contributors)<br>
<small>[Github](https://github.com/aarongough/mini-smtp-server)</small>

-----------------------------------------------------------
>### Sinatra (Ruby)
[Sinatra Crew](https://github.com/orgs/sinatra/people)<br>
[All Contributors](https://github.com/sinatra/sinatra/graphs/contributors)<br>
<small>[Github](https://github.com/sinatra/sinatra)</small>

-----------------------------------------------------------
>### Sinatra Formkeeper (Ruby)
[Lyo Kato](https://github.com/lyokato)<br>
[All Contributors](https://github.com/lyokato/sinatra-formkeeper/graphs/contributors)<br>
<small>[Github](https://github.com/lyokato/sinatra-formkeeper)</small>

-----------------------------------------------------------
>### Jimson (Ruby)
[Chris Kite](https://github.com/chriskite)<br>
[All Contributors](https://github.com/chriskite/jimson/graphs/contributors)<br>
<small>[Github](https://github.com/chriskite/jimson)</small>

-----------------------------------------------------------
>### RiotJS (Javascript)
[RiotJS Team](https://github.com/orgs/riot/people)<br>
[All Contributors](https://github.com/riot/riot/graphs/contributors)<br>
<small>[Github](https://github.com/riot/riot)</small>


-----------------------------------------------------------
>### Material Design Lite (Javascript)
[All Contributors](https://github.com/google/material-design-lite/graphs/contributors)<br>
<small>[Github](https://github.com/google/material-design-lite)</small>

