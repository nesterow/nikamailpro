JSON RPC
--------

### Address
```
    http://127.0.0.2:12080
```

------------------------------------------------------------------------------

#### Find Users
Method: `finduser("query")`

Returns: `Array[]`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"finduser","params":["ma"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```


#### Add Users
Method: `adduser("name", "password")`

Returns: `String`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"adduser","params":["mario","1234"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```

#### Remove Users
Method: `deluser("name", "password")`

Returns: `String`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"deluser","params":["mari"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```

#### Change User Password
Method: `password("name", "password")`

Returns: `String`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"password","params":["mari", "123123"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```

#### Add Email Alias
Method: `setalias("john+sales@mydomain.com", "john@mydomain.com")`

Returns: `String`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"setalias","params":["john+sales@mydomain.com", "john@mydomain.com"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```

#### Remove Email Alias
Method: `delalias("john+sales@mydomain.com")`

Returns: `String`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"delalias","params":["john+sales@mydomain.com"]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```

#### Get Host List
Method: `domains()`

Returns: `Array[]`

Example:
```sh
    curl --data-binary '{"jsonrpc":"2.0","id":"test","method":"domains","params":[]}' -H 'content-type:text/plain;' http://127.0.0.2:12080
```