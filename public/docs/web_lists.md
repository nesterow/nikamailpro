MANAGE SUBSCRIPTIONS
---------------------

**NikaMail** can broadcast messages to *subscription lists* and *private groups*.


### Subscription Lists

In NikaMail emails sent to subscription lists are broadcasted though a **relay address**.
Whenever you create a subscription list NikaMail assigns a service email address related to subscription list.
Whenever you need to broadcast an email you just send it to **relay address** and it will be distributed among subscribers.

### Create A Listing

#### 1. Give appropriate name for new listing

![Screenshot2](images/web_addlisting1.png)

#### 2. Select listing type:

![Screenshot2](images/web_addlisting2.png)

#### 3. Assign name for email box. **Only name, not an email address**

![Screenshot2](images/web_addlisting3.png)

#### 4. Confirm changes:

![Screenshot2](images/web_addlisting4.png)


### Manage Listing

#### 1. Open editing dialog:

![Screenshot2](images/web_listingmg1.png)

#### 2. Add new member to the subscribers list:

![Screenshot2](images/web_listingmg2.png)

You can add as much subscribers as you want, but it is best to integrate [Rest API](dev_rest) into your site in order to add members automatically.

![Screenshot2](images/web_listingmg3.png)


### Adding Authorized Sender
Only one user can broadcast emails to subscribers. Authorized sender is identified by e-mail address.
* Don't worry subscribers receive neither relay address or sender's address.
Subscribers receive emails from the listing mailbox such as news@foo.com. 

#### Open settings tab and input authorized sender email address:
![Screenshot2](images/web_listingmg4.png)


#### Always Press Save!
After you finished editing always press the save button, otherwise all changes will be lost.


#### Broadcasting Emails

Simply send an email to the relay address such as 'subscription-dohpogk@foo.com' and it will be delivered to subscribers.

You need to get access credentials in order to be able to broadcast emails, read subscriber's feedback and manage listing trough RestAPI.
There is only one password for everything related to a listing. Namely: `relay address`, `listing mailbox`, and `Rest API` have the same password.

* WARNING FOR DEVELOPERS: Don't use RestAPI in the front-end applications! By doing that you'll provide free access to mailbox password.


![Screenshot2](images/web_listingmg5.png)

#### Communicate With Subscribers
Subscription address such as 'news@foo.com' is a simple mailbox so you can access subscribers' feedback and communicate with them using any e-mail client.
By default the password is the same as for `relay address` or `Rest API`, but you can change it in mailbox manager.

