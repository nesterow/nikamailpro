REST API - Mailing Lists
-------------------------
WARNING DEVELOPERS: DO NOT USE THIS API IN FRONTEND APPLICATIONS. USE IT ONLY ON SERVER SIDE.

NikaMail allows to add/remove new members to subscription lists.

REST API is intended for internal use by your web applications **on server side** in order to add new members into subscription lists and groups.

#### Endpoint URL

```
    https://{ip_address}:10080/api/{subscription_mailbox}?password={mailbox_password}
```

### Examples

>##### Get Members

GET `https://{IP}:10080/api/{mailbox}?password={password}`

Response: `JSON Array[]`


```
    curl -k  https://127.0.0.2:10080/api/subscription?password=1234
```

-----------------------------------------------------------------
>##### Add Member

POST `https://{IP}:10080/api/{mailbox}?password={password}`

Header: `Content-Type: application/json;`

Request Body: `{"address": "EMAIL ADDRESS"}`

```
    curl -k -H "Content-Type: application/json" -X POST -d '{"address": "Some Dude <dude@some.com>"}' https://127.0.0.2:10080/api/subscribe?password=1234
```


-----------------------------------------------------------------
>##### Delete Member

DELETE `https://{IP}:10080/api/{mailbox}?password={password}`

Header: `Content-Type: application/json;`

Request Body: `{"address": "EMAIL ADDRESS"}`

```
    curl -k -H "Content-Type: application/json" -X DELETE -d '{"address": "Some Dude <dude@some.com>"}' https://127.0.0.2:10080/api/subscribe?password=1234
```