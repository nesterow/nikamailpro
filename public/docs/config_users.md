Managing Users Manually
-----------------------
There are two methods of managing users in NikaMail: 1. Using WebUI. 2. Manually using debug console.

#### Using debug console

##### Starting console
If you use docker simply type:
```
    ~# make console
```

Otherwise accessing console a bit tricky:
```
    ~# bin/jruby app/main.rb console
```

##### Invoke methods
Debug console is simly a Ruby IRB with access to NikaMail environment.
So the "commands" are actual ruby methods and must be used accordingly:
```ruby
    (irb)> mymethod "argument"  # RIGHT
    (irb)> mymethod  argument   # WRONG
    
    (irb)> mymethod "arg1", "arg2" # RIGHT
    (irb)> mymethod  arg1 arg2     # WRONG
```

##### Add an user
Method `adduser("name","password")`
```ruby
    (irb)> adduser 'mario', 'password123'
```

##### Change password
Method `password("name","password")`
```ruby
    (irb)> password 'mario', 'password1234'
```

##### Delete user password
Method `password("name")`
```ruby
    (irb)> deluser 'mario'
```

##### Search users
Method `finduser("query")`
```ruby
    (irb)> finduser 'ma'
```

##### Add alias
Method `setalias("alias email", "target email")`
```ruby
    (irb)> setalias 'mario+support@mysite.com', 'mario@mysite.com'
```

##### Delete alias
Method `delalias("alias email")`
```ruby
    (irb)> delalias 'mario+support@mysite.com'
```

### Apply Configuration
When you add/remove users you don't need to restart server to apply changes. However, when you add aliases you need to restart server.

Exit debug console by typing `exit` and start server in normal mode.
