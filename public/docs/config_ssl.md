Configure SSL
-----------------------
NikaMail uses Java Key Store format in order to keep SSL certificates.
The same certificate is used by StartTLS (Mireka) and WebUI to provide https.
The default keystore is included in the `config` directory but we recommend to replace it with your own.

### Generating a self-signed certificate
If you don't need to provide public services creating a self-signed cerificate is pretty enough.
You need a Java SE to be able to generate keystores with `keytool` command.

Copy following command to commnd line and press enter, then replace `keystore.jks` with generated file.
```
    keytool -genkey -keyalg RSA -alias servercert -dname "CN=1, OU=2, O=2, L=4, S=5, C=GB" -keystore keystore.jks  -storepass password -keypass password -validity 1440 -keysize 2048 -noprompt
```

### Importing authority signed certificate
There are several ways to generate a keystore from authority signed certificate.
The fastest is to use [Keystore Explorer](http://keystore-explorer.org/).
The orthodox way is to use [`openssl` and `keytool`](https://docs.oracle.com/cd/E35976_01/server.740/es_admin/src/tadm_ssl_convert_pem_to_jks.html) to convert PEM keys to JKS format