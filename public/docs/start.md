### GETTING STARTED

#### 1. DOWNLOAD
Download latest NikaMail distribution.


#### 2. INSTALL
Unpack distribution in directory where you want to keep emails.

```
    ~# tar -xf NikaMail.tar.xz
    ~# cd NikaMail
```

Remove Postfix and other email servers installed by default
```
   ~# apt remove postfix
```

*If you expect large number of e-mails with attachments we recommend to run it on a separate disk.*

*If you install it on Raspberry Pi we recommend to use a separate USB drive. Leave SD card to run system.*

Ensure if the files in `bin` directory have executable bit.
```sh
    ~# chmod u+x ./bin/*
```

#### 3. RUN WITH DOCKER

Install Docker on your system
```sh
    ~# apt install docker.io
    ~# systemctl start docker
```
Build image
```sh
    ~# make build
```

Run image
```sh
    ~# make start
```

#### RUN WITHOUT DOCKER
If docker isn't available on your system you can run it manually or using supervisor.

REQUIREMENTS:
1. Java JDK 7+
2. Linux/Mac or other POSIX system

Before running, recompile java extensions:
```
    ~# make build-java
```

Starting server
```
    ~# bin/jruby app/main.rb
```
Starting debug console
```
    ~# bin/jruby app/main.rb console
```

###### USING SUPERVISOR
1. Rename `config/supervisor.conf-template` to `config/supervisor.conf`
2. Edit `config/supervisor.conf` according to your server settings
3. Include absolute path to `config/supervisor.conf` into you supervisor configuration.
4. Restart supervisor


