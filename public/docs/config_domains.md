Configure DNS Records
-----------------------
Most mail exchange servers verify domain name before accepting incoming emails in order to reject spam.
It is done by checking SPF or DKIM DNS records. Public mail services such as 'gmail.com' won't accept
emails if DNS record doesn't exists or ip address of MTA doesn't match SPF record.

### Configuring SPF
Usually having SPF record is pretty enough for emails from you server to reach destination.

Add a TXT record to your domain configuration with following content replacing IP address with your server public address:
```
    v=spf1 ip4:192.168.0.1 -all
```

### Configuring DKIM
DKIM isn't supported by default however there are plans to add it near future. Please send me some feedback if you need DKIM support and it may speed up development.