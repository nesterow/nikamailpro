MANAGE USERS
------------

### Adding/Removing Users
In **NikaMail** a mailbox name is a user name. *NikaMail* doesn't support multi-domain configuration and
a user recieves emails to one `mailbox` disregarding domain name.
For example, if you allow more than one hostname (foo.com and bar.com) `user@foo.com` would be the same as `user@bar.com`.

#### Create A Mailbox
Press `add user` -&gt; Enter `Name` -&gt; Input `Password` -&gt; Press `Save`

![Screenshot](images/web_addbox.png)

#### Update A Mailbox
Press pencil icon -&gt; Input new mailbox `Password` -&gt; Press `Save`

![Screenshot](images/web_updatebox.png)


#### Delete A Mailbox
Press pencil icon -&gt; Press Delete -&gt; Confirm delete action

![Screenshot](images/web_delbox.png)