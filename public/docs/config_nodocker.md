Running with a process manager
-------------------------------
Docker is recommended way to run NikaMail,
but if you don't have such option you can use your favorite process manager.

##### CONS
* WebUI most likely won't support restart action. You'll have to restart server manually when required.


##### Requirements
* Only requirement is Java 8+ and a POSIX compatible system

#### START NIKAMAIL
```
    ~# cd /path/to/nikamail
    ~# bin/jruby app/main.rb
```
