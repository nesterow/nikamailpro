Running With Docker
-----------------------
Docker is recommended way to run NikaMail.

`Makefile` provides predefined commands to build and run docker image.

#### Build Docker Image
Following commands will rebuild Java extensions and build a docker image
```sh
    ~# make build
```

#### Run debug console with docker
```
    ~# make console
```

#### Run image in daemon mode
Following command runs image with 'always restart' policy.
The server will restart after host reboots
```
    ~# make start
```

#### Stop or restart daemon
```
    ~# make stop/restart
```

#### Stop manually
When the host has reboots the container will loose assigned name and `make stop/restart` won't work.
In that case use `docker stop [Id]` in order to stop container
```
    ~# docker ps
    ~# docker stop [Id]
```
