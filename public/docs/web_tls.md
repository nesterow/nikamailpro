CONFIGURE TLS/SSL
-----------------
**NikaMail** uses Java Key Store `*.jks` format to keep public and private keys for StartTLS and WebUI.

### Using a self-signed certificates.
Most email servers on the internet will accept self signed certificates when connecting to your server to pass an email,
but there is a certain possibility that some of them wouldn't give such server a high trust score.

However, using a self signed cert is pretty enough if you don't need to receive large amounts of correspondence or your server is for internal use.

### Generate A self-signed certificate

* Install Java Development Kit if you don't have it
* In the terminal call following command:

In this example: alias is `servercert` and password is `password`. You need to change it.
```
    ~# keytool -genkey -keyalg RSA -alias servercert -dname "CN=1, OU=2, O=2, L=4, S=5, C=GB" -keystore keystore.jks  -storepass password -keypass password -validity 1440 -keysize 2048 -noprompt
```
* Now replace `config/keystore.jks` with newly generated key.

* Or if you prefer to use WebUI:

![Screensho3](images/web_jks.png)

NOTE: For convenience NikaMail uses one password for both: keystore and private key. Tech-savvy people can change it if needed.

* Restart server after changing certificates.


### Use authority signed certificate

**There is a great utility for this purpose:  [Keystore Explorer](http://keystore-explorer.org/)**

* Download Keystore Explorer
* Import RSA keys
* Remember that password should be the same for both: keystore and private key
* Save `*.jks` file
* Upload newly generated key to NikaMail

There are various ways to generate a JKS file from an authority signed certificate. Most of them look like voodoo magic even for orthodox linux freaks.
If you want to learn some voodoo - google is glad to help you.





