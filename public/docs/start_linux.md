Installing On Linux
-----------------------
Execute following commands:
```
    ~# cd /path/to/nikamail
    ~# chmod u+x ./bin/*
    ~# sudo bin/install.sh
```

After installation visit `https://{IP}:10080` in order to finish configuration
