PAIRING WITH GMAIL
------------------

If there's a need to receive lots of emails and advertise your address publicly
you probably need to setup spam filters and `imap` inboxes. Setting up an own imap
server would make sense for companies with 20+ people on board.

Using gmail as a frontend client is more reasonable if you have a small team.
GMail will filter spam and keep your correspondence in one place while your team can use own brand.


### 1. Connect POP3 account to Gmail

* Open `Gmail Settings` and select `Accounts and import` view

![Screenshot1](images/web_gmail1.png)


* Click on `Import mail and contacts` and enter your email address.

![Screenshot1](images/web_gmail2.png)

* Then enter your mailbox password and press continue

![Screenshot1](images/web_gmail3.png)

* Gmail will try to retrieve emails from your inbox and this mailbox will be added to your gmail.



### 2. Connect SMTP account to Gmail

* Return to Gmail settings view and click `Save email as:` -&gt; `Add another account`. Then input your password and check `Using TLS`.

![Screenshot1](images/web_gmail5.png)


#### All Done!

Now you can use Gmail instead of email client.