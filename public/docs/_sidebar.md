<!-- docs/_sidebar.md -->
* [About](/)
- [Credits](credits.md)
* Install
    - [Getting Started](start.md)
    - [License BY-SA 4.0](license.md)

* WebUI
    - [First Start](web_install.md)
    - [Setting up DNS](web_domains.md)
    - [Setting up TLS](web_tls.md)
    - [Managing Users](web_users.md)
    - [Subscription Lists](web_lists.md)
    - [E-mail Clients](web_clients.md)
    - [Pairing with GMail](web_gmail.md)
    

* Manual Configuration
    - [Edit Configuration](config_files.md)
    - [Managing Users](config_users.md)
    - [Configure DNS](config_domains.md)
    - [StartTLS](config_ssl.md)
    - [Runing with Docker](config_docker.md)
    - [Runnig without Docker](config_nodocker.md)


* Dev Docs
    - [JSON RPC](dev_rpc.md)
    - [Mailbox Extensions](dev_ruby.md)
    - [Java Extensions](dev_java.md)
    - [Listing REST API](dev_rest.md)
    
    