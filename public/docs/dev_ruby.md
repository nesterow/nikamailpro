MAILBOX EXTENSIONS
---------------

**NikaMail** allows to handle incoming emails by writing *post-processing* hooks in Ruby.

You can do practically everything with emails,
for example: Redirect emails, Parse content, Write to database, Hadle attached files, and more..

### Registering hooks
A mailbox hook is simply a Ruby method which receives a parsed `.eml` file when it is delivered to mailbox.

In order to register a hook edit `app/hooks.rb`. A hook is related to *mailbox name*, you can add more than one hook and they will be executed in order.
```ruby
#app/hooks.rb
def self.registry
    return @registry if @registry
    @registry={
      
      mario: [ method(:print_mario_emails)] # <- here is the hooks for 'mario'
    
    }
    load_extensions()
    return @registry
end
def self.print_mario_emails(eml)
    puts "Post-processing is here"
    puts "
      From: #{eml.From}
      Subject: #{eml.Subject}
      SubjectID: #{eml.SubjectID}
      Date: #{eml.Date}
      Files: #{eml.Files.map {|f| f.filename}}
      Body: #{eml.Body.Content}
    "
end
```

### Parsing Raw Emails
NikaMail provides a parser for raw `*.eml` files. The class `Eml` is available globally and you can use it in your mailbox extensions.
By default all hooks receive a pased email as first argument.

------------------------------------------------------------------------
## class Eml


>###### Method: `new(String file, Bool raw)`
Creates an email instance from `file path` or `raw data`. By default flag `raw` is false.

-----------------------------------------------------------
>###### Method: `copy(String mailbox_1, String mailbox_2)`
Copy email to another mailbox.

-----------------------------------------------------------
>###### Method: `setTo(String address)`
Set email receiver

-----------------------------------------------------------
>###### Method: `setFrom(String address)`
Set email sender

-----------------------------------------------------------
>###### Method: `setSubject(String subject)`
Set email subject

-----------------------------------------------------------
>###### @Attribute: `From`
Canonical email address: "John Doe &lt;johndoe@lost.com&gt;"

-----------------------------------------------------------
>###### @Attribute: `AddressFrom`
Email address: "johndoe@lost.com"

-----------------------------------------------------------
>###### @Attribute: `To`
Canonical email address: "Jane Doe &lt;janedoe@lost.com&gt;"

-----------------------------------------------------------
>###### @Attribute: `AddressTo`
Email address: "janedoe@lost.com"

-----------------------------------------------------------
>###### @Attribute: `Subject`
Email subject

-----------------------------------------------------------
>###### @Attribute: `SubjectID`
An Id retrieved from inside square parentheses. If subject is `Message [123456122]` the id would be `123456122`

-----------------------------------------------------------
>###### @Attribute: `Body`
Email body

-----------------------------------------------------------
>###### @Attribute: `Files`
Email attachments (Array[])

-----------------------------------------------------------
>###### @Attribute: `raw`
Raw email



#### Usage Example:

```ruby
email = Eml.new(path)
  
sender = email.From
attachments = email.Files
pdf = attachments[0]
puts pdf.filename

email.setFrom('reports@myhost.com')
email.setSubject('PDF document 123')
email.copy([box1, box2])
```

--------------------------------------------

INSTALL RUBY GEMS
-----------------
If you need a third party library use following command to install ruby gems.
```
bin/jruby -S gem install awesomegem
```