CONFIGURING DNS
---------------

### 1. Add MX record
Add MX record pointing to your server
Example:
```
mx IN A 192.211.52.70
@ MX mx.nika.run.
```

    
### 2. Add SPF record
Most public services wont accept emails if your domain doesn't have a SPF record
Example:
```
@ IN TXT v=spf1 ip4:192.211.52.70 -all
```

### 3. Setup DKIM
Some public services prefer DKIM over SPF as more secure verification mechanism. It is believed that emails signed using this method have higher trust score in public services like Gmail.

* First of all you need to generate an RSA key pair. You can use an online tool for that purpose, but it is highly recommended to use `openssl`.
If you use MacOS or Linux, just open terminal and paste following commands.

```
    ~# openssl genrsa -out dkim.key 1024
    ~# openssl rsa -in dkim.key -pubout -out dkim.txt
```
`dkim.txt` include a public key, you need to delete lines starting with "---------" and format public key in one line.

* Afterwards copy following line in the beginnig of the line:

```
v=DKIM1;k=rsa;s=email;t=s;p=
```

* Resulting line has to look similar to following:

```
v=DKIM1;k=rsa;s=email;t=s;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0EXE+oUJpVZILAoIvBxwC6MPbV8ICxsktT4WweF1pAuDdQJvEBlmZT5AEFbT61jTzEhfDlcZQk9QQYbCmUNmDZCOjjgwCPAeDWasktz0OzsExTcKNWUOLLCPPEGXqNYY4HhCVBkqJ+7+2hkMfXTkMUReCvtnM2bL2W8x3eZiQewIDAQAB
```

* Add DNS record

```
mail._domainkey	IN TXT v=DKIM1;k=rsa;s=email;t=s;p=MIGfM..pubkey

```
NOTE: *NikaMail* selector is `mail._domainkey`. Currently there's no way to change it trough web interface, but tech-savvy people can edit 'app/lib/mta.rb' to change this value. 


* Add DKIM private key to NikaMail
    1. Open setting in the WebUI
    2. Select `dkim.key` from your disk
    3. Enter admin password and save and restart


![Screensho3](images/web_dkim.png)
