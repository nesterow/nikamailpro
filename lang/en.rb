module Lang
add :en, "English"
def self.en
{ 
	:name => "English",
	:forms => {
		:login => LoginForm,
		:settings => SettingsForm
	},

	:dialogs => {
		:confirm_title => "Are you sure?",
		:unrevertable => "You won't be able to revert this!",
		:delete_it => "Yes, delete it!",
		:delete_success_title => 'Deleted',
		:delete_success => 'Record has been deleted.',
		:error => 'Error',
		:server_error => "Server error"
	},

	:menu => {
		:home => "Home",
		:boxes => "Mail Boxes",
		:departments => "Mailing Lists",
		:documentation => "Documentation",
		:processing => "Extensions",
		:settings => "Settings",
		:manage => "Manage",
		:accounts => "Accounts",
		:logout => "Logout",
		:login => "Login"
	},

	:user => {
		:form_fail => "Please fill out form correctly.\n - Username must be at least 5 characters\n - Password must be at least 5 characters.",
		:wrong_password => "Wrong username or password",
		:login_success => "Login success",
		:register_success => "Registration success",
		:duplicate_email => "Sorry. Email '%s' already registered in our system",
		:duplicate_username => "Sorry. Login '%s' has already been taken.",
		:confirm_email_sent => "We've sent new confiration email to your inbox. Follow instructions in the email in order to confirm your address.",
		:confirm_wrong_email => "Something went wrong. Perhaps wrong email",
		:confirm_email_ok => "Thank you! Your email is confirmed.",
		:resend_email => "Resend email",
		:create_form_fail => "Please fill out form correctly.\n - Username must be at least 5 characters\n - Password must be at least 5 characters. \n-Email cannot be empty.",
		:created => "User created",
		:updated => "User updated",
		:deleted => "User deleted",
		:signin => "Login",
		:signup => "Sign Up"
	},

	:variables => {
		:duplicate => "Variable must be unique"
	},

	:settings => {
		:title => "CONFIGURATION",
		:password_required => "Domain and Admin password are required",
		:password_dont_match => "(Change password) Passwords don't match",
		:keystore_pass_required =>"Keystore password and Certificate alias are required"
	},
	
	:lists => {
		title: "MANAGE MAILING LISTS",
		list_name: 'List Name',
		list_name_tip: 'Enter a unique list name',
		list_name_required: 'Listing name is required!',
		subscribe_list_name: 'Subscription List (emails distributed through a relay address)',
		private_group_name: 'Private Group (all members can broadcast messages to group)',
		listing_type_title: 'Select listing type',
		email_box_name: 'Email Box Name',
		email_box_tip: 'only mailbox name, not an email address',
		email_box_required: 'Email box name is required',
		create_new: 'CREATE NEW LISTING',
		create_btn: 'Create!',
		add: 'add listing',
		next: 'Next',
		no_lists: 'NO LISTINGS YET',
		no_lists_tip: 'Manage subscription lists or private groups',
		type_subscribe: 'SUBSCRIPTION LIST',
		type_private: 'PRIVATE GROUP',
		edit_subscribe_desc: 'Subscribers receive emails from this address',
		edit_group_desc: 'Members send emails to this address in order to broadcast messages to other members',
		subscribers: "Subscribers",
		settings: 'Settings',
		rest: 'Rest API',
		delete: 'Delete',
		add_member: 'ADD NEW MEMBER',
		name: 'Name',
		email: 'Email Address',
		add_member: '+ Add Member',
		add_cancel: 'Cancel',
		search: 'Search..',
		add: 'add',
		not_found: 'No subscribers listed yet',
		relay: 'Relay Address',
		relay_tip: 'Emails sent to this address will be distributed among subscribers',
		relay_tip_private: 'Emails sent to this address are relayed to group subscribers. Only group members can send emails to this address.',
		sender: 'Authorized Sender',
		sender_tip_1: "Specify authorized sender email address. If there's no authorized sender then all emails sent to",
		sender_tip_2: "won't be processed",
		remove_listing: 'Remove Listing',
		remove_listing_warning: 'WARNING! This action will remove all listing data.',
		remove_listing_tip: '*Type "i want to delete it" in order to confirm removal.',
		remove_remove: 'remove',
		control_word: 'i want to delete it',
		input_name_email: 'Input name and email',
		success: "Success",
	},
	
	:users => {
		add_user: 'add user',
		search: 'Search..',
		address: 'Address',
		num_emails: 'Processed e-mails',
		action: 'Action',
		box: 'E-mail Box',
		new_btn: 'New',
		edit_btn: 'Edit',
		name: "Name",
		name_err: "Only following characters are allowed: numbers, lowercase letters, +, -, .",
		password: 'Password',
		save: "save",
		delete: "delete",
		added: 'Mailbox added',
		sure: 'Are you sure?',
		sure_tip: "You won't be able to revert this!",
		yes_delete: "Yes, delete it!",
		no_delete: "No, cancel!",
		deleted: 'Deleted!',
		deleted_tip: 'Mailbox has been deleted.',
	}

}
end
end