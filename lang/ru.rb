module Lang
add :ru, "Русский"
def self.ru
{ 
	:name => "Русский",
	:forms => {
		:login => LoginFormRu,
		:settings => SettingsFormRu
	},

	:dialogs => {
		:confirm_title => "Вы уверены?",
		:unrevertable => "Это действие будет невозможно отменить!",
		:delete_it => "Удалить!",
		:delete_success_title => 'Удалено',
		:delete_success => 'Запись была удалена',
		:error => 'Ошибка',
		:server_error => "Сервер не смог выполнить запрос"
	},

	:menu => {
		:home => "Главная",
		:boxes => "Адреса",
		:departments => "Рассылки",
		:documentation => "Документация",
		:processing => "Расширения",
		:settings => "Настройки",
		:manage => "Управление",
		:accounts => "Пользователи",
		:logout => "Выход",
		:login => "Логин"
	},
	:user => {
		:form_fail => "Форма заполнена неправильно.\n - Логин должен быть не менее 5-ти символов\n - Пароль должен быть не менее 5-ти символов.",
		:wrong_password => "Неправильный логин или пароль",
		:login_success => "Вход выполнен",
		:register_success => "Пользователь зарегистрирован",
		:duplicate_email => "Этот адрес уже зарегистрирован '%s' ",
		:duplicate_username => "Логин '%s' уже занят",
		:confirm_email_sent => "Мы отправили вам е-майл для подверждения регистрации",
		:confirm_wrong_email => "Ошибка. Неправильный е-майл",
		:confirm_email_ok => "Адрес подтвержден. Спасибо за регисрацию",
		:resend_email => "Отправить снова",
		:create_form_fail => "Форма заполнена неправильно.- Логин должен быть не менее 5-ти символов\n - Пароль должен быть минимум 5-ть символов. \n-Нужно указать емайл.",
		:created => "Пользователь создан",
		:updated => "Запись обнавлена",
		:deleted => "Пользователь удален",
		:signin => "Вход",
		:signup => "Регисрация"
	},

	:variables => {
		:duplicate => "Имя переменной должно быть уникальным"
	},

	:settings => {
		:title => "НАСТРОЙКИ СЕРВЕРА",
		:password_required => "Имя Домена и Пароль админа нужны для внесения изменений",
		:password_dont_match => "(Смена пароля не удалась) Пароли несовпадают",
		:keystore_pass_required =>"Введите имя сертификата и пароль приватного ключа"
	},
	
	:lists => {
		title: "УПРАВЛЕНИЕ РАССЫЛКАМИ",
		list_name: 'Имя Рассылки',
		list_name_tip: 'Введите имя рассылки',
		list_name_required: 'Введите имя рассылки',
		subscribe_list_name: 'Подписка (рассылка отправляется через специальный почтовый яшик)',
		private_group_name: 'Закрытая Группа (участники группы будут рассылать почту друг-другу)',
		listing_type_title: 'Выберите Тип Рассылки',
		email_box_name: 'Имя почтового ящика',
		email_box_tip: 'только имя, не надо вводить полный адрес (@)',
		email_box_required: 'Нужно ввести имя',
		create_new: 'СОЗДАТЬ НОВУЮ РАССЫКУ',
		create_btn: 'Создать',
		add: 'добавить рассылку',
		next: 'Далее',
		no_lists: 'НЕТ НАСТРОЕННЫХ РАССЫЛОК',
		no_lists_tip: 'Добавте новую рассылку',
		type_subscribe: 'ПОДПИСКА',
		type_private: 'ЗАКРЫТАЯ ГРУППА',
		edit_subscribe_desc: 'С этого адреса подписчики получают рассылку',
		edit_group_desc: 'Участники группы рессылают сообщения с попмощью этого адреса',
		subscribers: "Подписки",
		settings: 'Настройки',
		rest: 'RestAPI',
		delete: 'Удаление',
		add_member: 'ДОБАВИТЬ ПОСПИСЧИКА',
		name: 'Имя',
		email: 'Адрес почты',
		add_member: '+ Добавить',
		add_cancel: 'Отмена',
		search: 'Поиск..',
		add: 'добавить',
		not_found: 'Пустой Список',
		relay: 'Реле',
		relay_tip: 'Сообщения отправленые на этот адрес будут доставлены подписчикам',
		relay_tip_private: 'Сообщения отправленные на этот адрес будут доставлены участникам группы',
		sender: 'Адрес Автора Рассылки',
		sender_tip_1: "Только автор рассылки может отправлять сообщения. Если автор не указан тогда все сообщения отправленные на",
		sender_tip_2: "не будут разосланы",
		remove_listing: 'Удалить',
		remove_listing_warning: 'ЭТО ДЕЙСТВИЕ УДАЛИТ ВСЕ ДАННЫЕ РАССЫЛКИ',
		remove_listing_tip: '*Введите "я хочу удалить" чтобы подвердить ваше намерение.',
		remove_remove: 'удалить',
		control_word: 'я хочу удалить',
		input_name_email: 'введите имя и почтовый адрес',
		success: "Операция выполнена",
	},
	
	:users => {
		add_user: 'Добавить Ящик',
		search: 'Поиск..',
		address: 'Адрес',
		num_emails: 'Статистика',
		action: 'Действие',
		box: 'Почтовый Ящик',
		new_btn: 'Новый',
		edit_btn: 'Измененить',
		name: "Имя",
		name_err: "Только цифры, латинские буквы, +, -, .",
		password: 'Пароль',
		save: "сохранить",
		delete: "удалить",
		added: 'Добавлено',
		sure: 'Вы уверены?',
		sure_tip: "Это действие нельзя отменить!",
		yes_delete: "Да, я уверен!",
		no_delete: "Нет, отмена!",
		deleted: 'Удалено!',
		deleted_tip: 'Ящик был удален.',
	}
}
end
end