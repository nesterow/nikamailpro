=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

require "ostruct"
require "json"

module Lang

	def self.add lang, name
		@list ||= []
		@list.push({
			:lang => lang,
			:name => name
		})
	end

	def self.struct(data)
		res = JSON.parse(data.to_json, object_class: OpenStruct)
		res.forms = OpenStruct.new(data[:forms])
		return res
	end

	def self.list
		@list
	end

	def self.get(l=nil)
		return en if l.nil?
		begin
			l = l.to_s
			if l.length <= 3 
				lang = send(l)
			else
				lang = en
			end
			return lang
		rescue Exception => e
			return en
		end
	end

	def self.msg(l=nil)
		return struct(en) if l.nil?
		begin
			l = l.to_s
			if l.length <= 3 
				lang = send(l)
			else
				lang = en
			end
			return struct(lang)
		rescue Exception => e
			return struct(en) 
		end
		
	end

end

module Lang
	Dir["#{File.dirname(__FILE__)}/forms/*.rb"].each {|file| 
		require file
	}
end

Dir["#{File.dirname(__FILE__)}/*.rb"].each {|file| 
	require file unless file.include? 'lang.rb'
}