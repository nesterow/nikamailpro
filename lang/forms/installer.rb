class PreconfigForm < MDLForm
	title "CONFIGURATION"
	action "/install"
	method "POST"
	redirect "/install/settings"
	select(
		option("English", :en),
		option("Русский", :ru),
		:title => "Language",
		:name => :language
	)
	input "Domain", :type => :text, :name => :domain, :required => true, :error => "Input domain name"
	input "Admin Username", :type => :text, :name => :username, :required => true, :error => "Username is required"
	input "Admin Password", :name=> :password, :type => :password, :required => true, :error => "Password is required"
	input '', type: :hidden, name: 'action', value: 'general'
	button "Next"
end

class ServerPreconfigForm < MDLForm
	title "SERVER CONFIG"
	action "/install"
	method "POST"
	redirect "/install/finish"
	checkbox "SMTP MX Server (port 25)", :name => :mx
	checkbox "SMTP MTA Server (port 587)", :name => :mta
	checkbox "POP3 Server (port 110)", :name => :pop
	input '', type: :hidden, name: 'action', value: 'server'
	button "Finish"
end