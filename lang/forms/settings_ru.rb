class SettingsFormRu < MDLForm
	title "НАСТОЙКИ"
	id "settings"
	action "/settings"
	redirect "/restart"
	method "POST"
	text "Выберите Язык"
	select(
		option("English", :en),
		option("Русский", :ru),
		:title => "Язык",
		:name => :language
	)
	text "Доменное Имя"
	input "Домен", :type => :text, :name => :domain, :required => true, :error => "Input domain name"
	#input "Admin Username", :type => :text, :name => :username, :required => true, :error => "Username is required"
	
	text "Сменить Пароль"
	input "Новый Пароль", :name=> :new_password, :type => :password
	input "Повторить Пароль", :name=> :repeat_new_password, :type => :password
	
	text "Настройки Сервера"
	checkbox "Получать Почту (SMTP 25-й порт)", :name => :mx
	checkbox "Отправлять Почту (SMTP 587-й порт)", :name => :mta
	checkbox "Сервер Почтового Клиента (POP3 110-й порт)", :name => :pop
	
	br
	br
	
	text "SSL/TLS Ключи шифрования"
	input "Файл ключей (JKS)", :name=> :keystore, :type => :file
	input "Имя сертификата", :name=> :keystore_alias, :type => :text, :required => true
	input "Пароль ", :name=> :keystore_password, :type => :password, :required => true
	
	br
	br
	
	text "Настройки DKIM"
	checkbox "Включить DKIM", :name => :dkim
	input "RSA Приватный Ключ", :name=> :dkim_key, :type => :file
	
	br
	br
	text "Введите Текущий Пароль"
	input "Пароль админа", :name=> :password, :type => :password, :required => true, :error => "Пароль нужен для сохранения изменений"

	button "Сохранить"
end