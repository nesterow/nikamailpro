class SettingsForm < MDLForm
	title "SETTINGS"
	id "settings"
	action "/settings"
	redirect "/restart"
	method "POST"
	text "Select language"
	select(
		option("English", :en),
		option("Русский", :ru),
		:title => "Language",
		:name => :language
	)
	text "Domain name"
	input "Domain", :type => :text, :name => :domain, :required => true, :error => "Input domain name"
	#input "Admin Username", :type => :text, :name => :username, :required => true, :error => "Username is required"
	
	text "Change password"
	input "New Password (optional)", :name=> :new_password, :type => :password
	input "Repeat Password (optional)", :name=> :repeat_new_password, :type => :password
	
	text "Server settings"
	checkbox "SMTP MX Server (port 25)", :name => :mx
	checkbox "SMTP MTA Server (port 587)", :name => :mta
	checkbox "POP3 Server (port 110)", :name => :pop
	
	br
	br
	
	text "Change SSL/TLS Keys"
	input "Keystore (JKS)", :name=> :keystore, :type => :file
	input "Certificate Name", :name=> :keystore_alias, :type => :text, :required => true
	input "Keystore Password", :name=> :keystore_password, :type => :password, :required => true
	
	br
	br
	
	text "DKIM Settings"
	checkbox "Turn on DKIM", :name => :dkim
	input "Private Key", :name=> :dkim_key, :type => :file
	
	br
	br
	text "Enter current password"
	input "Admin Password", :name=> :password, :type => :password, :required => true, :error => "Password is required to commit changes"

	button "Save & Reboot"
end