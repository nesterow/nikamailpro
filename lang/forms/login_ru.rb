
class LoginFormRu < MDLForm
	id :loginform
	title "Логин"
	action "/login"
	method "POST"
	redirect "/"
	input "Логин", :type => :text, :name => :username, :required => true, :error => "Введите логин"
	input "Пароль", :name=> :password, :type => :password, :required => true, :error => "Введите пароль"
	button "Войти"
end

class RegistrationFormRu < MDLForm
	title ""
	action "/signup"
	method "POST"
	redirect "/login"
	input "Логин", :type => :text, :name => :username, :required => true, :error => "Введите логин"
	input "Почта", :type => :email, :name => :email, :required => true, :error => "Введите почту"
	input "Пароль", :name=> :password, :type => :password, :required => true, :error => "Введите пароль"
	button "Зарегистрировать"
end