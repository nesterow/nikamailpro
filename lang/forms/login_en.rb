
class LoginForm < MDLForm
	id :loginform
	title "Login"
	action "/login"
	method "POST"
	redirect "/"
	input "Username", :type => :text, :name => :username, :required => true, :error => "Username is required"
	input "Password", :name=> :password, :type => :password, :required => true, :error => "Password is required"
	button "Login"
end

class RegistrationForm < MDLForm
	title ""
	action "/signup"
	method "POST"
	redirect "/login"
	input "Username", :type => :text, :name => :username, :required => true, :error => "Username is required"
	input "Email", :type => :email, :name => :email, :required => true, :error => "Email is required"
	input "Password", :name=> :password, :type => :password, :required => true, :error => "Password is required"
	button "Sign Up"
end