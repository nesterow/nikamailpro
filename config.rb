=begin

  Anton A. Nesterov (c) 2018, CC-BY-SA 4.0
  License: https://creativecommons.org/licenses/by-sa/4.0/

=end

module Kernel
	require_relative "./utils/forms/mdl"
	require_relative "./utils/views/table"
	require_relative "./lang/lang"

	ROOT_PATH = File.dirname(__FILE__).gsub('/web', '')

	PRODUCTION = false


	HTTP_HOST = "0.0.0.0"
	HTTP_PORT = 10080


end